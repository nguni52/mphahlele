package za.ac.mphahlele.config;


import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import za.ac.mphahlele.App;
import za.ac.mphahlele.data.MongoConfig;

@Configuration
@ComponentScan(basePackageClasses = App.class, excludeFilters = @ComponentScan.Filter({Controller.class, Configuration.class}))
public class AppConfig {
    @Bean
    public static PropertyPlaceholderConfigurer placeholderConfigurer() {
        PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = MongoConfig.placeholderConfigurer();
        return propertyPlaceholderConfigurer;
    }
}