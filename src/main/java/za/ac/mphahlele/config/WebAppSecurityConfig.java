package za.ac.mphahlele.config;

import org.springframework.core.annotation.Order;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by nguni52 on 2014/08/08.
 */
@Order(1)
public class WebAppSecurityConfig extends AbstractSecurityWebApplicationInitializer {
}