package za.ac.mphahlele.user.service;

public interface SecurityService {
    public static final String ROLE_USER = "USER";
    public static final String ROLE_ADMIN = "ADMIN";
}
