package za.ac.mphahlele.user.service;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import za.ac.mphahlele.data.repository.UserRepository;
import za.ac.mphahlele.model.User;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by nguni52 on 2014/08/08.
 */
public class UserService implements UserDetailsService {
    @Inject
    private UserRepository userRepository;

    @Inject
    private PasswordEncoder passwordEncoder;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = getUser(username);
        if (user == null) {
            throw new UsernameNotFoundException("user not found");
        }
        return createUser(user);
    }

    private User getUser(String username) {
        return userRepository.findByUsername(username);
    }

    public void login(User user) {
        SecurityContextHolder.getContext().setAuthentication(authenticate(user));
    }

    private Authentication authenticate(User user) {
        return new UsernamePasswordAuthenticationToken(createUser(user), null, createAuthority(user));
    }

    public org.springframework.security.core.userdetails.User createUser(User user) {
        return new org.springframework.security.core.userdetails.User(user.getUsername(),
                user.getPassword(), createAuthority(user));
    }

    private Collection<GrantedAuthority> createAuthority(User user) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (String role : user.getRole()) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }

    public User save(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    public boolean isAuthenitcated() {
        Authentication auth = getAuthentication();
        return auth != null && auth.isAuthenticated();
    }

    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext()
                .getAuthentication();
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public boolean hasAuthority(String role) {
        GrantedAuthority authority = new SimpleGrantedAuthority(role);
        return getAuthentication().getAuthorities().contains(authority);
    }
}
