package za.ac.mphahlele.village.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.ac.mphahlele.data.repository.HouseHoldRepository;
import za.ac.mphahlele.data.repository.VillageRepository;
import za.ac.mphahlele.exception.EntityExistsException;
import za.ac.mphahlele.model.HouseHold;
import za.ac.mphahlele.model.Village;

import javax.inject.Inject;
import java.util.Collection;

@Service
public class VillageServer {
    private VillageRepository villageRepository;
    private HouseHoldRepository houseHoldRepository;

    @Inject
    public VillageServer(VillageRepository villageRepository) {
        this.villageRepository = villageRepository;
    }

    @Inject
    public void setHouseHoldRepository(HouseHoldRepository houseHoldRepository) {
        this.houseHoldRepository = houseHoldRepository;
    }

    @Transactional
    public void create(Village village) {
        throwExceptionIfVillageExists(village);
        save(village);
    }

    public void throwExceptionIfVillageExists(Village village) {
        Village existingVillage = getVillage(village.getId());
        if (existingVillage != null) {
            throw new EntityExistsException("Village with name: " + village.getName() +
                    " already exists.");
        }
    }

    public void save(Village village) {
        villageRepository.save(village);
    }

    public Village getVillage(String id) {
        return villageRepository.findById(id);
    }

    public Collection<Village> getAll() {
        return villageRepository.findAll();
    }

    public void update(Village village) {
        save(village);
    }

    public Collection<HouseHold> getHouseholdForVillage(String village) {
        return houseHoldRepository.findByVillage(village);
    }
}
