package za.ac.mphahlele.village.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.ac.mphahlele.household.main.controller.HouseHoldController;
import za.ac.mphahlele.model.HouseHold;
import za.ac.mphahlele.model.Village;
import za.ac.mphahlele.user.service.SecurityService;

import java.util.Collection;

@Controller
@Secured(SecurityService.ROLE_USER)
@RequestMapping(DetailVillageController.VILLAGE_DETAILS_NAME)
public class DetailVillageController extends VillageController {
    public static final String VILLAGE_DETAILS = "village/details";
    public static final String VILLAGE_DETAILS_NAME = VILLAGE_DETAILS + "/{"+ID+"}";

    @RequestMapping(method = RequestMethod.GET)
    public String getDetails(@PathVariable(ID) String id, Model model) {
        Village village = villageServer.getVillage(id);
        Collection<HouseHold> houseHolds = villageServer.getHouseholdForVillage(village.getName());
        model.addAttribute(HouseHoldController.HOUSEHOLDS, houseHolds);
        addAttributes(model, village);
        return VILLAGE_DETAILS;
    }
}
