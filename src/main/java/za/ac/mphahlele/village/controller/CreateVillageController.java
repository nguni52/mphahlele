package za.ac.mphahlele.village.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import za.ac.mphahlele.user.service.SecurityService;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Village;
import za.ac.mphahlele.support.MessageHelper;

import javax.validation.Valid;

@Controller
@Secured({SecurityService.ROLE_ADMIN})
@RequestMapping(value = CreateVillageController.VILLAGE_CREATE)
public class CreateVillageController extends VillageController {
    public static final String VILLAGE_CREATE = "village/create";

    @RequestMapping(method = RequestMethod.GET)
    public String getForm(Model model) {
        Village village = new Village();
        return addAttributesAndReturnForm(model, village);
    }

    private String addAttributesAndReturnForm(Model model, Village village) {
        addAttributes(model, village);
        return VILLAGE_CREATE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute(VILLAGE) Village village,
                       BindingResult result,
                       Model model,
                       RedirectAttributes ra) {
        if (result.hasErrors()) {
            return addAttributesAndReturnForm(model, village);
        }
        try {
            villageServer.create(village);
            MessageHelper.addSuccessAttribute(ra, "village.save.success");
            return MainController.REDIRECT +  ListVillageController.VILLAGE_LIST;
        } catch (Exception e) {
            result.addError(new ObjectError(VILLAGE, "Error occurred while saving village! " + e.getMessage()));
            return addAttributesAndReturnForm(model, village);
        }
    }
}
