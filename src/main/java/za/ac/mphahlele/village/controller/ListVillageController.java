package za.ac.mphahlele.village.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = ListVillageController.VILLAGE_LIST)
public class ListVillageController extends VillageController {
    public static final String VILLAGE_LIST = "village/list";
    public static final String VILLAGES = "villages";

    @RequestMapping(method = RequestMethod.GET)
    public String getForm(Model model) {
        model.addAttribute(VILLAGES, villageServer.getAll());
        return VILLAGE_LIST;
    }
}
