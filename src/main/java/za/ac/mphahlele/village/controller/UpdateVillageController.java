package za.ac.mphahlele.village.controller;

import org.apache.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Village;
import za.ac.mphahlele.support.MessageHelper;
import za.ac.mphahlele.user.service.SecurityService;

import javax.validation.Valid;

@Controller
@Secured(SecurityService.ROLE_ADMIN)
@RequestMapping(value=UpdateVillageController.VILLAGE_UPDATE_ID)
public class UpdateVillageController extends VillageController {
    public static final String VILLAGE_UPDATE = "village/update";
    public static final String VILLAGE_UPDATE_ID = VILLAGE_UPDATE + MainController.HOME + "{" + ID +"}";
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @RequestMapping(method = RequestMethod.GET)
    public String getForm(@PathVariable(ID) String id, Model model) {
        logger.info("GETTING A VILLAGE");
        Village village = villageServer.getVillage(id);
        logger.info("VILLAGE: " + village.toString());
        return addAttributesAndReturnForm(model, village);
    }

    private String addAttributesAndReturnForm(Model model, Village village) {
        addAttributes(model, village);
        return VILLAGE_UPDATE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute(VILLAGE) Village village,
                       BindingResult result,
                       Model model,
                       RedirectAttributes ra) {
        if(result.hasErrors()) {
            return addAttributesAndReturnForm(model, village);
        }
        try {
            villageServer.update(village);logger.info("UPDATING VILLAGE: " + village);
            MessageHelper.addSuccessAttribute(ra, "village.update.success");
            logger.info("Successful save for village");
            return MainController.REDIRECT + ListVillageController.VILLAGE_LIST;
        } catch(Exception e) {
            result.addError(new ObjectError(VILLAGE, "Error occurred while updating village! " + e.getMessage()));
            return addAttributesAndReturnForm(model, village);
        }
    }
}
