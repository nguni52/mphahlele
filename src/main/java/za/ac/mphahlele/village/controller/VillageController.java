package za.ac.mphahlele.village.controller;

import org.springframework.ui.Model;
import za.ac.mphahlele.model.Village;
import za.ac.mphahlele.village.service.VillageServer;

import javax.inject.Inject;

public class VillageController {
    public static final String VILLAGE = "village";
    public static final String VILLAGES = "villages";
    public static final String ID = "id";

    @Inject
    protected VillageServer villageServer;

    public void addAttributes(Model model, Village village) {
        model.addAttribute(VILLAGE, village);
    }
}
