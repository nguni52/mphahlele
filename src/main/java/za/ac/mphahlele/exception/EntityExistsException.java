package za.ac.mphahlele.exception;

public class EntityExistsException extends RuntimeException {
    public EntityExistsException(String message) {
            super(message);
        }
}
