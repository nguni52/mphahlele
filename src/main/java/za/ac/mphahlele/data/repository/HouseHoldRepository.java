package za.ac.mphahlele.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.ac.mphahlele.model.HouseHold;

import java.util.Collection;

public interface HouseHoldRepository extends MongoRepository<HouseHold, String> {
    HouseHold findByIdNumber(String idNumber);

    Collection<HouseHold> findByVillage(String village);
}
