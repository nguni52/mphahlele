package za.ac.mphahlele.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.ac.mphahlele.model.User;

/**
 * Created by nguni52 on 2014/08/08.
 */
public interface UserRepository extends MongoRepository<User, String>  {
    User findByUsername(String username);
}
