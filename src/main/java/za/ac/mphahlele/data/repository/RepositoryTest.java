package za.ac.mphahlele.data.repository;


import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.mongodb.core.MongoTemplate;

import javax.inject.Inject;
import java.io.IOException;

public abstract class RepositoryTest {
    private static Logger logger = Logger.getLogger(RepositoryTest.class.getName());

    @Inject
    protected MongoTemplate template;

    @BeforeClass
    public static void startServer() {
        try {
            EmbeddedMongoServer.initializeDB();
        } catch (IOException e) {
            logger.debug("IOEXCEPTION IN REPOSITORY TEST: ", e);
        }
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
