package za.ac.mphahlele.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.ac.mphahlele.model.Church;

public interface ChurchRepository extends MongoRepository<Church, String> {

    Church findByApplicantIdentityNumber(String applicantIdNumber);
}
