package za.ac.mphahlele.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.ac.mphahlele.model.Village;

public interface VillageRepository extends MongoRepository<Village, String> {
    Village findByName(String villageName);
    Village findById(String id);
}
