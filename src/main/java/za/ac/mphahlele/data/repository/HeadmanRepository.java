package za.ac.mphahlele.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.ac.mphahlele.model.Headman;

public interface HeadmanRepository extends MongoRepository<Headman, String> {
    Headman findByIdNumber(String idNumber);
    Headman findByName(String headman);
}
