package za.ac.mphahlele.data.repository;

import de.flapdoodle.embed.mongo.Command;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.config.RuntimeConfigBuilder;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.config.IRuntimeConfig;
import de.flapdoodle.embed.process.runtime.Network;

import java.io.IOException;

public class EmbeddedMongoServer {
    private static final int DEFAULT_TEST_PORT = 27028;//27017;
    private static MongodExecutable mongodExecutable;
    private static volatile int numberOfTestSuites = 0;
    private static volatile boolean notStarted = true;

    public static synchronized void initializeDB() throws IOException {
        if (numberOfTestSuites == 0 && notStarted) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        IMongodConfig mongodConfig = new MongodConfigBuilder()
                                .version(Version.Main.PRODUCTION)
                                .net(new Net(DEFAULT_TEST_PORT, Network.localhostIsIPv6()))
                                .build();

                        Command command = Command.MongoD;

                        IRuntimeConfig runtimeConfig = new RuntimeConfigBuilder()
                                .defaults(command)
                                .build();

                        MongodStarter runtime = MongodStarter.getInstance(runtimeConfig);
                        mongodExecutable = runtime.prepare(mongodConfig);
                        mongodExecutable.start();
                        notStarted = false;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    do {
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } while (numberOfTestSuites > 0);

                }
            }).run();
        }
        numberOfTestSuites++;
    }
}
