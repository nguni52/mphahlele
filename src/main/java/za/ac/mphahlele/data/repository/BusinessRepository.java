package za.ac.mphahlele.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.ac.mphahlele.model.Business;

public interface BusinessRepository  extends MongoRepository<Business, String> {

    Business findByApplicantIdentityNumber(String applicantIdentityNumber);
}
