package za.ac.mphahlele.data.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.ac.mphahlele.model.Levy;

import java.util.Collection;

public interface LevyRepository extends MongoRepository<Levy, String> {
    public Collection<Levy> findByHouseholdIdNumber(String householdIdNumber);
    public Levy findById(String id);
}
