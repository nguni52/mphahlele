package za.ac.mphahlele.data.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import za.ac.mphahlele.model.Dependant;

import java.util.List;

public interface DependantRepository extends MongoRepository<Dependant, String> {
    public List<Dependant> findByHouseholdIdNumber(String householdIdNumber);
    public Dependant findByIdNumber(String idNumber);
}
