package za.ac.mphahlele.data;

import com.mongodb.Mongo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoFactoryBean;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import javax.inject.Inject;

/**
 * Created by nguni52 on 2014/06/18.
 */
@Configuration
@EnableMongoRepositories
@ComponentScan(basePackageClasses = DataApp.class)
public class MongoConfig  extends AbstractMongoConfiguration {
    public static final String PROPS = "/mongo.properties";
    @Value("${mongo.database.name}")
    private String dbName;
    @Value("${mongo.host}")
    private String host;
    @Value("${mongo.port}")
    private Integer port;

    private
    @Inject
    Mongo mongo;

    @Override
    protected String getDatabaseName() {
        return dbName;
    }

    @Override
    public Mongo mongo() throws Exception {
        return mongo;
    }

    @Bean
    protected MongoFactoryBean mongoFactoryBean() throws Exception {
        MongoFactoryBean mongoFactoryBean = new MongoFactoryBean();
        mongoFactoryBean.setHost(host);
        mongoFactoryBean.setPort(port);
        return mongoFactoryBean;
    }

    @Bean
    protected MongoDbFactory defaultMongoDBFactory() {
        return new SimpleMongoDbFactory(mongo, dbName);
    }

    @Bean
    public static PropertyPlaceholderConfigurer placeholderConfigurer() {
        PropertyPlaceholderConfigurer placeholderConfigurer = new PropertyPlaceholderConfigurer();
        placeholderConfigurer.setLocation(new ClassPathResource(PROPS));
        return placeholderConfigurer;
    }

    @Override
    protected String getMappingBasePackage() {
        return "za.ac.mphahlele.model";
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }
}
