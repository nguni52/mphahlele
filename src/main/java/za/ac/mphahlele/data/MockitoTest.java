package za.ac.mphahlele.data;

import org.junit.Before;
import org.mockito.MockitoAnnotations;

public abstract class MockitoTest {
    @Before
    public void initMocks() throws Exception {
        MockitoAnnotations.initMocks(this);
    }
}
