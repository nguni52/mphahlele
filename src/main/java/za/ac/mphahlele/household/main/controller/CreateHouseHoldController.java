package za.ac.mphahlele.household.main.controller;

import org.apache.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.HouseHold;
import za.ac.mphahlele.model.Village;
import za.ac.mphahlele.support.MessageHelper;
import za.ac.mphahlele.user.service.SecurityService;
import za.ac.mphahlele.village.controller.VillageController;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@Secured({SecurityService.ROLE_ADMIN})
@RequestMapping(value = CreateHouseHoldController.HOUSEHOLD_CREATE)
public class CreateHouseHoldController extends HouseHoldController {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    public static final String HOUSEHOLD_CREATE = "household/create";


    @RequestMapping(method = RequestMethod.GET)
    public String getForm(Model model) {
        HouseHold houseHold = new HouseHold();
        Collection<Village> villages = villageServer.getAll();
        model.addAttribute(VillageController.VILLAGES, villages);

        return addAttributesAndReturnForm(model, houseHold);
    }

    private String addAttributesAndReturnForm(Model model, HouseHold houseHold) {
        addAttributes(model, houseHold);
        return HOUSEHOLD_CREATE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute(HOUSEHOLD) HouseHold houseHold,
                       BindingResult result,
                       Model model,
                       RedirectAttributes ra) {
        if (result.hasErrors()) {
            return addAttributesAndReturnForm(model, houseHold);
        }
        try {
            houseHoldServer.create(houseHold);
            MessageHelper.addSuccessAttribute(ra, "household.save.success");
            return MainController.REDIRECT +  ListHouseHoldController.HOUSEHOLD_LIST;
        } catch (Exception e) {
            result.addError(new ObjectError(HOUSEHOLD, "Error occurred while saving household! " + e.getMessage()));
            return addAttributesAndReturnForm(model, houseHold);
        }
    }
}
