package za.ac.mphahlele.household.main.controller;

import org.springframework.ui.Model;
import za.ac.mphahlele.household.main.service.HouseHoldServer;
import za.ac.mphahlele.model.HouseHold;
import za.ac.mphahlele.village.service.VillageServer;

import javax.inject.Inject;

public class HouseHoldController {
    public static final String HOUSEHOLD = "household";
    public static final String HOUSEHOLDS = "households";
    public static final String ID_NUMBER = "idNumber";
    public static final String HOUSEHOLD_ID_NUMBER = "householdIdNumber";


    @Inject
    protected HouseHoldServer houseHoldServer;

    @Inject
    protected VillageServer villageServer;

    public void addAttributes(Model model, HouseHold houseHold) {
        model.addAttribute(HOUSEHOLD, houseHold);
    }
}
