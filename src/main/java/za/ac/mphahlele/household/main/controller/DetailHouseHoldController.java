package za.ac.mphahlele.household.main.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.ac.mphahlele.household.dependant.service.DependantServer;
import za.ac.mphahlele.household.levy.service.LevyServer;
import za.ac.mphahlele.model.HouseHold;
import za.ac.mphahlele.user.service.SecurityService;

import javax.inject.Inject;

@Controller
@Secured(SecurityService.ROLE_USER)
@RequestMapping(DetailHouseHoldController.HOUSEHOLD_DETAILS_ID_NUMBER)
public class DetailHouseHoldController extends HouseHoldController {
    public static final String HOUSEHOLD_DETAILS = "household/details";
    public static final String HOUSEHOLD_DETAILS_ID_NUMBER = HOUSEHOLD_DETAILS + "/{"+ID_NUMBER+"}";
    public static final String DEPENDANTS = "dependants";
    public static final String LEVIES = "levies";

    @Inject
    private DependantServer dependantServer;
    @Inject
    private LevyServer levyServer;

    @RequestMapping(method = RequestMethod.GET)
    public String getDetails(@PathVariable(ID_NUMBER) String idNumber, Model model) {
        HouseHold houseHold = houseHoldServer.getHouseHold(idNumber);
        model.addAttribute(DEPENDANTS, dependantServer.findDependantsByHouseHoldIdNumber(idNumber));
        model.addAttribute(LEVIES, levyServer.findLeviesByHouseHoldIdNumber(idNumber));
        addAttributes(model, houseHold);
        return HOUSEHOLD_DETAILS;
    }
}
