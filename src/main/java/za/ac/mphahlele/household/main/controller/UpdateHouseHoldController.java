package za.ac.mphahlele.household.main.controller;

import org.apache.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.HouseHold;
import za.ac.mphahlele.model.Village;
import za.ac.mphahlele.support.MessageHelper;
import za.ac.mphahlele.user.service.SecurityService;
import za.ac.mphahlele.village.controller.VillageController;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@Secured(SecurityService.ROLE_ADMIN)
@RequestMapping(value=UpdateHouseHoldController.HOUSEHOLD_UPDATE_ID)
public class UpdateHouseHoldController extends HouseHoldController {
    public static final String HOUSEHOLD_UPDATE = "household/update";
    public static final String HOUSEHOLD_UPDATE_ID = HOUSEHOLD_UPDATE + MainController.HOME +
            "{"+ID_NUMBER+"}";
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @RequestMapping(method = RequestMethod.GET)
    public String getForm(@PathVariable(ID_NUMBER) String idNumber, Model model) {
        HouseHold houseHold = houseHoldServer.getHouseHold(idNumber);
        Collection<Village> villages = villageServer.getAll();
        model.addAttribute(VillageController.VILLAGES, villages);
        return addAttributesAndReturnForm(model, houseHold);
    }

    private String addAttributesAndReturnForm(Model model, HouseHold houseHold) {
        addAttributes(model, houseHold);
        return HOUSEHOLD_UPDATE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute(HOUSEHOLD) HouseHold houseHold,
                       BindingResult result,
                       Model model,
                       RedirectAttributes ra) {
        if(result.hasErrors()) {
            return addAttributesAndReturnForm(model, houseHold);
        }
        try {
            houseHoldServer.update(houseHold);
            MessageHelper.addSuccessAttribute(ra, "houseHold.update.success");
            return MainController.REDIRECT +  ListHouseHoldController.HOUSEHOLD_LIST;
        } catch(Exception e) {
            result.addError(new ObjectError(HOUSEHOLD, "Error occurred while updating household! " + e.getMessage()));
            return addAttributesAndReturnForm(model, houseHold);
        }
    }

}
