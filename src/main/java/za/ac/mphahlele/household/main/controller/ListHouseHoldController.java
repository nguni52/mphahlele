package za.ac.mphahlele.household.main.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value=ListHouseHoldController.HOUSEHOLD_LIST)
public class ListHouseHoldController extends HouseHoldController {
    public static final String HOUSEHOLD_LIST = "household/list";
    public static final String HOUSEHOLDS = "households";

    @RequestMapping(method = RequestMethod.GET)
    public String getForm(Model model) {
        model.addAttribute(HOUSEHOLDS, houseHoldServer.getAll());
        return HOUSEHOLD_LIST;
    }
}
