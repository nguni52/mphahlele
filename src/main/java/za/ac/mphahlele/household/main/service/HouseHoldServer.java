package za.ac.mphahlele.household.main.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.ac.mphahlele.data.repository.HouseHoldRepository;
import za.ac.mphahlele.exception.EntityExistsException;
import za.ac.mphahlele.model.HouseHold;

import javax.inject.Inject;
import java.util.Collection;

@Service
public class HouseHoldServer {
    private HouseHoldRepository houseHoldRepository;

    @Inject
    public HouseHoldServer(HouseHoldRepository houseHoldRepository) {
        this.houseHoldRepository = houseHoldRepository;
    }

    @Transactional
    public void create(HouseHold houseHold) {
        throwExceptionIfHouseHoldExists(houseHold);
        save(houseHold);
    }

    public void throwExceptionIfHouseHoldExists(HouseHold houseHold) {
        HouseHold existingHouseHold = getHouseHold(houseHold.getIdNumber());
        if (existingHouseHold != null) {
            throw new EntityExistsException("HouseHold with id: " + houseHold.getIdNumber() +
                    " already exists.");
        }
    }

    public void save(HouseHold houseHold) {
        houseHoldRepository.save(houseHold);
    }

    public HouseHold getHouseHold(String idNumber) {
        return houseHoldRepository.findByIdNumber(idNumber);
    }

    public Collection<HouseHold> getAll() {
        return houseHoldRepository.findAll();
    }

    public void update(HouseHold houseHold) {
        save(houseHold);
    }
}
