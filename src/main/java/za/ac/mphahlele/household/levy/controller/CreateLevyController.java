package za.ac.mphahlele.household.levy.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.household.main.controller.DetailHouseHoldController;
import za.ac.mphahlele.household.main.controller.HouseHoldController;
import za.ac.mphahlele.model.Levy;
import za.ac.mphahlele.support.MessageHelper;
import za.ac.mphahlele.user.service.SecurityService;

import javax.validation.Valid;

@Controller
@Secured(SecurityService.ROLE_ADMIN)
@RequestMapping(value=CreateLevyController.LEVY_CREATE_HOUSEHOLD_ID_NUMBER)
public class CreateLevyController extends LevyController {
    public static final String LEVY_CREATE = "household/levy/create";
    public static final String LEVY_CREATE_HOUSEHOLD_ID_NUMBER = LEVY_CREATE + "/{" +
            HouseHoldController.HOUSEHOLD_ID_NUMBER + "}";

    @RequestMapping(method = RequestMethod.GET)
    public String getForm(@PathVariable(HouseHoldController.HOUSEHOLD_ID_NUMBER) String householdIdNumber,
                          Model model) {
        Levy levy = new Levy();
        levy.setHouseholdIdNumber(householdIdNumber);
        return addAttributesAndReturnForm(model, levy);
    }

    private String addAttributesAndReturnForm(Model model, Levy levy) {
        addAttributes(model, levy);
        return LEVY_CREATE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute(LEVY) Levy levy,
                       BindingResult result,
                       Model model,
                       RedirectAttributes ra) {
        if (result.hasErrors()) {
            return addAttributesAndReturnForm(model, levy);
        }
        try {
            levyServer.create(levy);
            MessageHelper.addSuccessAttribute(ra, "levy.save.success");
            return MainController.REDIRECT + DetailHouseHoldController.HOUSEHOLD_DETAILS + MainController.HOME +
                    levy.getHouseholdIdNumber();
        } catch (Exception e) {
            result.addError(new ObjectError(LEVY, "Error occurred while saving levy! " + e.getMessage()));
            return addAttributesAndReturnForm(model, levy);
        }
    }
}
