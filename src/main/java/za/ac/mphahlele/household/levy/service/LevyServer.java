package za.ac.mphahlele.household.levy.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.ac.mphahlele.data.repository.LevyRepository;
import za.ac.mphahlele.exception.EntityExistsException;
import za.ac.mphahlele.model.Levy;
import za.ac.mphahlele.model.Levy;

import javax.inject.Inject;
import java.util.Collection;

@Service
public class LevyServer {
    private LevyRepository levyRepository;

    @Inject
    public LevyServer(LevyRepository levyRepository) {
        this.levyRepository = levyRepository;
    }

    @Transactional
    public void create(Levy levy) {
        throwExceptionIfLevyExists(levy);
        save(levy);
    }

    public void throwExceptionIfLevyExists(Levy levy) {
        Levy existingLevy = getLevy(levy.getId());
        if (existingLevy != null) {
            throw new EntityExistsException("Levy with name: " + levy.getName() +
                    " already exists.");
        }
    }

    public void save(Levy levy) {
        levyRepository.save(levy);
    }

    public Levy getLevy(String idNumber) {
        return levyRepository.findById(idNumber);
    }

    public Collection<Levy> getAll() {
        return levyRepository.findAll();
    }

    public void update(Levy levy) {
        save(levy);
    }

    public Collection<Levy> findLeviesByHouseHoldIdNumber(String idNumber) {
        return levyRepository.findByHouseholdIdNumber(idNumber);
    }
}
