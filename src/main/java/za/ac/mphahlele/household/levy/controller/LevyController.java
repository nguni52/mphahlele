package za.ac.mphahlele.household.levy.controller;

import org.springframework.ui.Model;
import za.ac.mphahlele.household.levy.service.LevyServer;
import za.ac.mphahlele.model.Levy;

import javax.inject.Inject;

public class LevyController {
    public static final String LEVY_DETAILS_ID = "id";
    public static final String LEVY = "levy";
    @Inject
    LevyServer levyServer;

    protected void addAttributes(Model model, Levy levy) {
        model.addAttribute(LEVY, levy);
    }
}
