package za.ac.mphahlele.household.levy.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.household.levy.service.LevyServer;
import za.ac.mphahlele.model.Levy;
import za.ac.mphahlele.user.service.SecurityService;

import javax.inject.Inject;

@Controller
@Secured(SecurityService.ROLE_ADMIN)
@RequestMapping(value=DetailLevyController.LEVY_DETAILS_ID_NUMBER)
public class DetailLevyController extends LevyController {
    public final static String LEVY_DETAILS = "household/levy/details";
    public final static String LEVY_DETAILS_ID_NUMBER = LEVY_DETAILS + MainController.HOME +
            "{"+LEVY_DETAILS_ID+"}";

    @Inject
    private LevyServer levyServer;

    @RequestMapping(method = RequestMethod.GET)
    public String getDetails(@PathVariable(LEVY_DETAILS_ID) String id, Model model) {
        Levy levy = levyServer.getLevy(id);
        addAttributes(model, levy);
        return LEVY_DETAILS;
    }
}
