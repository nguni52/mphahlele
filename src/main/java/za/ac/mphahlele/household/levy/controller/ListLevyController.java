package za.ac.mphahlele.household.levy.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import za.ac.mphahlele.user.service.SecurityService;

@Controller
@Secured(SecurityService.ROLE_ADMIN)
public class ListLevyController extends LevyController {
}
