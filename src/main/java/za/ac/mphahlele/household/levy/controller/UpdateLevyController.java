package za.ac.mphahlele.household.levy.controller;

import org.apache.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.household.main.controller.DetailHouseHoldController;
import za.ac.mphahlele.model.Levy;
import za.ac.mphahlele.support.MessageHelper;
import za.ac.mphahlele.user.service.SecurityService;

import javax.validation.Valid;

@Controller
@Secured(SecurityService.ROLE_ADMIN)
@RequestMapping(value= UpdateLevyController.LEVY_UPDATE_ID_NUMBER)
public class UpdateLevyController extends LevyController {
    public static final String LEVY_UPDATE = "household/levy/update";
    public static final String LEVY_UPDATE_ID_NUMBER = LEVY_UPDATE + MainController.HOME + "{"+LEVY_DETAILS_ID+"}";
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @RequestMapping
    public String getForm(@PathVariable(LEVY_DETAILS_ID) String id, Model model) {
        logger.info("updating levy");
        Levy levy = levyServer.getLevy(id);
        return addAttributesAndReturnForm(model, levy);
    }

    private String addAttributesAndReturnForm(Model model, Levy levy) {
        addAttributes(model, levy);
        return LEVY_UPDATE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute(LEVY) Levy levy,
                       BindingResult result,
                       Model model,
                       RedirectAttributes ra) {
        if(result.hasErrors()) {
            return addAttributesAndReturnForm(model, levy);
        }
        try {
            levyServer.update(levy);logger.info("UPDATING LEVY: " + levy);
            MessageHelper.addSuccessAttribute(ra, "levy.update.success");
            logger.info("Successful save for village");
            return MainController.REDIRECT + DetailHouseHoldController.HOUSEHOLD_DETAILS + MainController.HOME +
                    levy.getHouseholdIdNumber();
        } catch(Exception e) {
            result.addError(new ObjectError(LEVY, "Error occurred while updating levy! " + e.getMessage()));
            return addAttributesAndReturnForm(model, levy);
        }
    }
}
