package za.ac.mphahlele.household.dependant.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.household.dependant.service.DependantServer;
import za.ac.mphahlele.model.Dependant;
import za.ac.mphahlele.user.service.SecurityService;

import javax.inject.Inject;

@Controller
@Secured(SecurityService.ROLE_ADMIN)
@RequestMapping(DetailDependantController.DEPENDANT_DETAILS_ID_NUMBER)
public class DetailDependantController extends DependantController {
    public static final String DEPENDANT_DETAILS = "household/dependant/details";
    public static final String DEPENDANT_DETAILS_ID_NUMBER = DEPENDANT_DETAILS + MainController.HOME + "{"+ID_NUMBER+"}";

    @Inject
    private DependantServer dependantServer;

    @RequestMapping(method = RequestMethod.GET)
    public String getDetails(@PathVariable(ID_NUMBER)String idNumber, Model model) {
        Dependant dependant = dependantServer.getDependant(idNumber);
        addAttributes(model, dependant);
        return DEPENDANT_DETAILS;
    }

}
