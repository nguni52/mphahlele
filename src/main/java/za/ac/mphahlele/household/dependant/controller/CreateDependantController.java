package za.ac.mphahlele.household.dependant.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.household.main.controller.DetailHouseHoldController;
import za.ac.mphahlele.household.main.controller.HouseHoldController;
import za.ac.mphahlele.model.Dependant;
import za.ac.mphahlele.support.MessageHelper;
import za.ac.mphahlele.user.service.SecurityService;

import javax.validation.Valid;

@Controller
@Secured(SecurityService.ROLE_ADMIN)
@RequestMapping(value = CreateDependantController.DEPENDANT_CREATE_HOUSEHOLD_ID_NUMBER)
public class CreateDependantController extends DependantController {
    public static final String DEPENDANT_CREATE = "household/dependant/create";
    public static final String DEPENDANT_CREATE_HOUSEHOLD_ID_NUMBER = DEPENDANT_CREATE + "/{"+
            HouseHoldController.HOUSEHOLD_ID_NUMBER+"}";


    @RequestMapping(method = RequestMethod.GET)
    public String getForm(@PathVariable(HouseHoldController.HOUSEHOLD_ID_NUMBER) String householdIdNumber,
                          Model model) {
        Dependant dependant = new Dependant();
        dependant.setHouseholdIdNumber(householdIdNumber);
        model.addAttribute("genders", MainController.getGenderList());
        return addAttributesAndReturnForm(model, dependant);
    }

    private String addAttributesAndReturnForm(Model model, Dependant dependant) {
        addAttributes(model, dependant);
        return DEPENDANT_CREATE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute(DEPENDANT) Dependant dependant,
                        BindingResult result,
                        Model model,
                        RedirectAttributes ra) {
        if (result.hasErrors()) {
            return addAttributesAndReturnForm(model, dependant);
        }
        try {
            dependantServer.create(dependant);
            MessageHelper.addSuccessAttribute(ra, "dependant.save.success");
            return MainController.REDIRECT + DetailHouseHoldController.HOUSEHOLD_DETAILS + MainController.HOME +
                    dependant.getHouseholdIdNumber();
        } catch (Exception e) {
            result.addError(new ObjectError(DEPENDANT, "Error occurred while saving dependant! " + e.getMessage()));
            return addAttributesAndReturnForm(model, dependant);
        }
    }
}