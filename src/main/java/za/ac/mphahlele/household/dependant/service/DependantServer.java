package za.ac.mphahlele.household.dependant.service;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.ac.mphahlele.data.repository.DependantRepository;
import za.ac.mphahlele.exception.EntityExistsException;
import za.ac.mphahlele.model.Dependant;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;

@Service
public class DependantServer {
    private DependantRepository dependantRepository;

    @Inject
    public DependantServer(DependantRepository dependantRepository) {
        this.dependantRepository = dependantRepository;
    }

    @Transactional
    public void create(Dependant dependant) {
        throwExceptionIfDependantExists(dependant);
        save(dependant);
    }

    private void save(Dependant dependant) {
        dependantRepository.save(dependant);
    }

    public void throwExceptionIfDependantExists(Dependant dependant) {
        Dependant existingDependant = getDependant(dependant.getIdNumber());
        if (existingDependant != null) {
            throw new EntityExistsException("Dependant with id: " + existingDependant.getIdNumber() +
                    " already exists.");
        }
    }

    public List<Dependant> findDependantsByHouseHoldIdNumber(String idNumber) {
        return dependantRepository.findByHouseholdIdNumber(idNumber);
    }

    public Dependant getDependant(String idNumber) {
        return dependantRepository.findByIdNumber(idNumber);
    }

    public Collection<Dependant> getAll() {
        return dependantRepository.findAll();
    }

    public void update(Dependant dependant) {
        save(dependant);
    }
}
