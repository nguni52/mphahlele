package za.ac.mphahlele.household.dependant.controller;

import org.apache.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.household.main.controller.DetailHouseHoldController;
import za.ac.mphahlele.household.main.controller.HouseHoldController;
import za.ac.mphahlele.model.Dependant;
import za.ac.mphahlele.support.MessageHelper;
import za.ac.mphahlele.user.service.SecurityService;

import javax.validation.Valid;

@Controller
@Secured(SecurityService.ROLE_ADMIN)
@RequestMapping(value=UpdateDependantController.DEPENDANT_UPDATE_ID_NUMBER)
public class UpdateDependantController extends DependantController {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    public static final String DEPENDANT_UPDATE = "household/dependant/update";
    public static final String DEPENDANT_UPDATE_ID_NUMBER = DEPENDANT_UPDATE + MainController.HOME +
            "{"+ HouseHoldController.ID_NUMBER+"}";

    @RequestMapping(method = RequestMethod.GET)
    public String getForm(@PathVariable(ID_NUMBER) String idNumber, Model model) {
        Dependant dependant = dependantServer.getDependant(idNumber);
        model.addAttribute("genders", MainController.getGenderList());
        return addAttributesAndReturnForm(model, dependant);
    }

    private String addAttributesAndReturnForm(Model model, Dependant dependant) {
        addAttributes(model, dependant);
        return DEPENDANT_UPDATE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute(DEPENDANT) Dependant dependant,
                       BindingResult result,
                       Model model,
                       RedirectAttributes ra) {
        if(result.hasErrors()) {
            return addAttributesAndReturnForm(model, dependant);
        }
        try {
            dependantServer.update(dependant);
            MessageHelper.addSuccessAttribute(ra, "dependant.update.success");
            return MainController.REDIRECT +  DetailHouseHoldController.HOUSEHOLD_DETAILS + MainController.HOME +
                    dependant.getHouseholdIdNumber();
        } catch(Exception e) {
            result.addError(new ObjectError(DEPENDANT, "Error occurred while updating dependant! " + e.getMessage()));
            return addAttributesAndReturnForm(model, dependant);
        }
    }
}
