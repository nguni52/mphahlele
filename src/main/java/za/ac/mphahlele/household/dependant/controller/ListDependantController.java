package za.ac.mphahlele.household.dependant.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import za.ac.mphahlele.user.service.SecurityService;

@Controller
@Secured(SecurityService.ROLE_ADMIN)
public class ListDependantController extends DependantController {
    public static final String DEPENDANT_LIST = "dependant/list";
}
