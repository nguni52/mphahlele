package za.ac.mphahlele.household.dependant.controller;

import org.springframework.ui.Model;
import za.ac.mphahlele.household.dependant.service.DependantServer;
import za.ac.mphahlele.model.Dependant;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nguni52 on 2014/08/17.
 */
public class DependantController {
    public static final String DEPENDANT = "dependant";
    public static final String ID_NUMBER = "idNumber";

    @Inject
    protected DependantServer dependantServer;

    public void addAttributes(Model model, Dependant dependant) {
        model.addAttribute(DEPENDANT, dependant);
    }
}
