package za.ac.mphahlele.builder;

import za.ac.mphahlele.model.Dependant;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by nguni52 on 2014/08/17.
 */
public class DependantBuilder {
    private String idNumber;
    private String householdIdNumber;
    private String surname;
    private String name;
    private String gender;
    private String occupation;
    private String relationshipToOwner;

    public DependantBuilder(String idNumber, String householdIdNumber, String surname, String name, String gender, String occupation,
                            String relationshipToOwner) {
        this.idNumber = idNumber;
        this.householdIdNumber = householdIdNumber;
        this.surname = surname;
        this.name = name;
        this.gender = gender;
        this.occupation = occupation;
        this.relationshipToOwner = relationshipToOwner;
    }

    public static Dependant buildADependant(String idNumber, String householdIdNumber, String surname, String name,
                                            String gender, String occupation, String relationshipToOwner) {
        return new Dependant(idNumber, householdIdNumber, surname, name, gender, occupation,
                 relationshipToOwner);
    }

    public static Dependant buildADependant(String householdIdNumber, int i) {
        String idNumber = "12345" + i;
        String surname = "Doe" + i;
        String name = "John" + i;
        String gender = (i%2 == 0) ? "Male":"Female";
        String occupation = "Student" + i;
        String relationshipToOwner = (i%2 == 0) ? "son":"daughter";
        return buildADependant(idNumber,householdIdNumber,surname,name,gender,occupation,relationshipToOwner);
    }

    public static Dependant buildADependant(int i) {
        String idNumber = "12345" + i;
        String householdIdNumber = "12344" + i;
        String surname = "Doe" + i;
        String name = "John" + i;
        String gender = (i%2 == 0) ? "male":"female";
        String occupation = "Student" + i;
        String relationshipToOwner = (i%2 == 0) ? "son":"daughter";
        return buildADependant(idNumber,householdIdNumber,surname,name,gender,occupation,relationshipToOwner);
    }

    public static Collection<Dependant> buildDependants(String houseHoldIdNumber, int numberOfDependants) {
        List<Dependant> dependants = new ArrayList<>();
        for(int i=0;i<numberOfDependants;i++) {
            dependants.add(buildADependant(houseHoldIdNumber, i));
        }
        return dependants;
    }
}
