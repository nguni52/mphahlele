package za.ac.mphahlele.builder;

import za.ac.mphahlele.model.Address;

public class AddressBuilder {

    private String street;
    private String suburb;
    private String city;
    private String postalCode;

    public AddressBuilder(String street, String suburb, String city, String postalCode) {
        this.street = street;
        this.suburb = suburb;
        this.city = city;
        this.postalCode = postalCode;
    }

    public static Address buildAnAddress() {
        return new AddressBuilder(
                "1 Jorissen Street",
                "Braamfontein",
                "Johannesburg",
                "2050"
        ).build();
    }

    public Address build() {
        Address address = new Address();
        address.setStreet(street);
        address.setSuburb(suburb);
        address.setCity(city);
        address.setPostalCode(postalCode);
        return address;
    }
}
