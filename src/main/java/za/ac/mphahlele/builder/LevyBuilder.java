package za.ac.mphahlele.builder;

import org.joda.time.DateTime;
import za.ac.mphahlele.model.Levy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by nguni52 on 2014/08/17.
 */
public class LevyBuilder {
    private DateTime dateAdded;
    private String id;
    private String householdIdNumber;
    private String name;
    private Double amount;

    public LevyBuilder(String id, String householdIdNumber, String name, Double amount, DateTime date) {
        this.id = id;
        this.householdIdNumber = householdIdNumber;
        this.name = name;
        this.amount = amount;
        this.dateAdded = date;
    }

    public static Levy buildALevy(String id, String householdIdNumber, String name, Double amount, DateTime date) {
        return new Levy(id, householdIdNumber, name, amount, date);
    }

    public static Levy buildALevy(String householdIdNumber, int i) {
        String id = "id"+i;
        String name = "name"+i;
        Double amount = 10 + new Double(i);
        DateTime dateAdded = new DateTime();
        return buildALevy(id, householdIdNumber, name, amount, dateAdded);
    }

    public static Levy buildALevy(int i) {
        String id = "id"+i;
        String householdIdNumber = "12344"+i;
        String name = "name"+i;
        Double amount = 10 + new Double(i);
        DateTime dateAdded = new DateTime();
        return buildALevy(id, householdIdNumber, name, amount, dateAdded);
    }

    public static Collection<Levy> buildLevies(String householdIdNumber, int numberOfLevies) {
        List<Levy> levies = new ArrayList<>();
        for(int i=0;i<numberOfLevies;i++) {
            levies.add(buildALevy(i));
        }
        return levies;
    }
}
