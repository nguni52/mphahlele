package za.ac.mphahlele.builder;


import za.ac.mphahlele.model.Address;
import za.ac.mphahlele.model.Headman;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class HeadmanBuilder {
    private String idNumber;
    private String name;
    private String gender;
    private String unitNumber;
    private String wardNumber;
    private boolean isAccessToElectricity;
    private boolean isAccessToWater;
    private Address postalAddress;

    public HeadmanBuilder(String idNumber, String name, String gender, String unitNumber, String wardNumber,
                          boolean isAccessToElectricity, boolean isAccessToWater, Address postalAddress) {
        this.idNumber = idNumber;
        this.name = name;
        this.gender = gender;
        this.unitNumber = unitNumber;
        this.wardNumber = wardNumber;
        this.isAccessToElectricity = isAccessToElectricity;
        this.isAccessToWater = isAccessToWater;
        this.postalAddress = postalAddress;
    }

    public static Headman buildAHeadMan(String idNumber, String name, String gender, String unitNumber, String wardNumber,
                                        boolean isAccessToElectricity, boolean isAccessToWater, Address postalAddress) {
        return new Headman(idNumber, name, gender, unitNumber, wardNumber, isAccessToElectricity, isAccessToWater, postalAddress);
    }

    public static Headman buildAHeadman(int i) {
        String idNumber = "123456" + i;
        String name = "headman" + i;
        String gender = "male";
        int unitNumber = i*5;
        int wardNumber = i*4;
        boolean isAccessToElectricity = false;
        boolean isAccessToWater = false;
        Address postalAddress = AddressBuilder.buildAnAddress();

        return buildAHeadMan(idNumber, name, gender, Integer.toString(unitNumber), Integer.toString(wardNumber),
                isAccessToElectricity, isAccessToWater, postalAddress);
    }

    public static Collection<Headman> buildHeadmen(int numberofHeadmen) {
        List<Headman> headmen = new ArrayList<>();
        for(int i=0;i<numberofHeadmen;i++) {
            headmen.add(buildAHeadman(i));
        }
        return headmen;
    }
}
