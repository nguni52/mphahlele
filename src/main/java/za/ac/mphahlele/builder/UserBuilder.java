package za.ac.mphahlele.builder;

import za.ac.mphahlele.model.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserBuilder {
    private String username;
    private String firstName;
    private String lastName;
    private String password;

    public UserBuilder(String username, String firstName, String lastName, String password) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
    }

    public static User buildAUser(String username, String firstName, String lastName, String password) {
        return new UserBuilder(username, firstName, lastName, password).build();
    }

    public static User buildAUser(int i) {
        String username = "username" + i;
        String firstName = "John" + i;
        String lastName = "Doe" + i;
        String password = "Password" + i + "#$";
        return buildAUser(username, firstName, lastName, password);
    }

    public static Collection<User> buildUsers(int numberOfUsers) {
        List<User> users = new ArrayList<User>();
        for (int i = 0; i < numberOfUsers; i++) {
            users.add(buildAUser(i));
        }
        return users;
    }

    private User build() {
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setUsername(username);
        user.setPassword(password);
        return user;
    }
}
