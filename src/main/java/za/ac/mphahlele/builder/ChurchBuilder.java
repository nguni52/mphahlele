package za.ac.mphahlele.builder;

import za.ac.mphahlele.model.Address;
import za.ac.mphahlele.model.Church;
import za.ac.mphahlele.model.ReceiverRevenue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ChurchBuilder {
    private String applicantSurname;
    private String applicantName;
    private String applicantIdentityNumber;
    private Address applicantAddress;
    private String nameOfChurch;
    private String place;
    private String standNo;
    private String siteSize;
    private String chairPersonRemarks;
    private ReceiverRevenue receiverRevenue;

    public ChurchBuilder(String applicantSurname, String applicantName, String applicantIdentityNumber,
                         Address applicantAddress, String nameOfChurch, String place, String standNo, String siteSize,
                         String chairPersonRemarks, ReceiverRevenue receiverRevenue) {
        this.applicantSurname = applicantSurname;
        this.applicantName = applicantName;
        this.applicantIdentityNumber = applicantIdentityNumber;
        this.applicantAddress = applicantAddress;
        this.nameOfChurch = nameOfChurch;
        this.place = place;
        this.standNo = standNo;
        this.siteSize = siteSize;
        this.chairPersonRemarks = chairPersonRemarks;
        this.receiverRevenue = receiverRevenue;
    }

    public static Church buildAChurch(String applicantSurname, String applicantName, String applicantIdentityNumber,
                                      Address applicantAddress, String nameOfChurch, String place, String standNo, String siteSize,
                               String chairPersonRemarks, ReceiverRevenue receiverRevenue) {
        return new Church(applicantSurname, applicantName, applicantIdentityNumber,
                applicantAddress, nameOfChurch, place, standNo, siteSize,
                chairPersonRemarks, receiverRevenue);
    }

    public static Church buildAChurch(int i) {
         String applicantSurname = "Doe" + i;
         String applicantName = "John" + i;
         String applicantIdentityNumber = "1234567899102";
         Address applicantAddress = AddressBuilder.buildAnAddress();
         String nameOfChurch = "Church Name" + i;
         String place = "place" + i;
         int standNo = i*5;
         int siteSize = i *20;
         String chairPersonRemarks = "Remarks"+i;
         ReceiverRevenue receiverRevenue = ReceiverRevenueBuilder.buidAReceiverRevenue();

        return buildAChurch(applicantName, applicantSurname, applicantIdentityNumber,
                applicantAddress, nameOfChurch, place, Integer.toString(standNo), Integer.toString(siteSize),
                chairPersonRemarks, receiverRevenue);
    }


    public static Collection<Church> buildChurches(int numberOfChurches) {
        List<Church> churches = new ArrayList<>();
        for(int i=0;i<numberOfChurches;i++) {
            churches.add(buildAChurch(i));
        }
        return churches;
    }
}
