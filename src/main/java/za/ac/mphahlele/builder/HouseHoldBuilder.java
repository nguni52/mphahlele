package za.ac.mphahlele.builder;


import za.ac.mphahlele.model.Address;
import za.ac.mphahlele.model.HouseHold;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class HouseHoldBuilder {
    private String idNumber;
    private String title;
    private String surname;
    private String fullName;
    private int standNo;
    private Address postalAddress;
    private int wardNumber;
    private boolean isAccessToElectricity;
    private boolean isAccessToWater;
    private double levyAmount;
    private String village;

    public HouseHoldBuilder(String idNumber, String title, String surname, String fullName, int standNo,
                            Address postalAddress, int wardNumber, boolean isAccessToElectricity,
                            boolean isAccessToWater, double levyAmount) {
        this.idNumber = idNumber;
        this.title = title;
        this.surname = surname;
        this.fullName = fullName;
        this.standNo = standNo;
        this.postalAddress = postalAddress;
        this.wardNumber = wardNumber;
        this.isAccessToElectricity = isAccessToElectricity;
        this.isAccessToWater = isAccessToWater;
        this.levyAmount = levyAmount;
    }

    public static HouseHold buildAHouseHold(String idNumber, String title, String surname, String fullName, int standNo,
                                        Address postalAddress, int wardNumber, boolean isAccessToElectricity,
                                        boolean isAccessToWater, double levyAmount) {
        return new HouseHoldBuilder(idNumber, title, surname, fullName, standNo, postalAddress, wardNumber,
                isAccessToElectricity, isAccessToElectricity, levyAmount).build();
    }

    public static HouseHold buildAHouseHold(int i) {
        String idNumber = "123456" + i;
        String title = "Mr.";
        String surname = "Doe" + i;
        String fullName = "John" + i;
        int standNo = i*5;
        int wardNumber = i*4;
        boolean isAccessToElectricity = false;
        double levyAmount = 10.0 * i;
        Address postalAddress = AddressBuilder.buildAnAddress();
        return buildAHouseHold(idNumber, title, surname, fullName, standNo, postalAddress, wardNumber,
                isAccessToElectricity, isAccessToElectricity, levyAmount);
    }

    private HouseHold build() {
        HouseHold houseHold =
                new HouseHold(idNumber, village, title, surname, fullName, Integer.toString(standNo),
                postalAddress, Integer.toString(wardNumber), isAccessToElectricity, isAccessToElectricity, levyAmount);


        return houseHold;
    }

    public static Collection<HouseHold> buildHouseHolds(int numberOfHouseHolds) {
        List<HouseHold> houseHolds = new ArrayList<>();
        for (int i = 0; i < numberOfHouseHolds; i++) {
            houseHolds.add(buildAHouseHold(i));
        }
        return houseHolds;
    }
}
