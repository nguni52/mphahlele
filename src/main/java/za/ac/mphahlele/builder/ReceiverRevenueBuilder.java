package za.ac.mphahlele.builder;

import za.ac.mphahlele.model.ReceiverRevenue;

public class ReceiverRevenueBuilder {
    private String name;
    private String receiptNo;
    private String signature;
    private String date;

    public ReceiverRevenueBuilder(String name, String receiptNo, String signature, String date) {
        this.name = name;
        this.receiptNo = receiptNo;
        this.signature = signature;
        this.date = date;
    }

    public static ReceiverRevenue buidAReceiverRevenue() {
        return new ReceiverRevenueBuilder("Receiver", "10", "My Signature", "20-05-2014").build();
    }

    public ReceiverRevenue build() {
        ReceiverRevenue receiverRevenue = new ReceiverRevenue();
        receiverRevenue.setName(name);
        receiverRevenue.setReceiptNo(receiptNo);
        receiverRevenue.setSignature(signature);
        receiverRevenue.setDate(date);
        return receiverRevenue;
    }
}
