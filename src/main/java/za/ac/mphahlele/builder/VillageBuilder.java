package za.ac.mphahlele.builder;

import za.ac.mphahlele.model.Village;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class VillageBuilder {
    private String name;
    private String headman;
    private String positionType;
    private String contactDetails;

    public VillageBuilder(String name, String headman, String positionType, String contactDetails) {
        this.name = name;
        this.headman = headman;
        this.positionType = positionType;
        this.contactDetails = contactDetails;
    }

    public static Village buildAVillage(String name, String headman, String positionType, String contactDetails) {
        return new VillageBuilder(name, headman, positionType, contactDetails).build();
    }

    public static Village buildAVillage(int i) {
        String name = "village" + i;
        String headman = "headman" + i;
        String positionType = "positionType" + i;
        String contactDetails = "contactDetails" + i;
        return buildAVillage(name, headman, positionType, contactDetails);
    }

    private Village build() {
        Village village = new Village();
        village.setName(name);
        village.setHeadman(headman);
        village.setPositionType(positionType);
        village.setContactDetails(contactDetails);
        return village;
    }

    public static Collection<Village> buildVillages(int numberOfVillages) {
        List<Village> villages = new ArrayList<>();
        for (int i = 0; i < numberOfVillages; i++) {
            villages.add(buildAVillage(i));
        }
        return villages;
    }
}
