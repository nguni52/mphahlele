package za.ac.mphahlele.builder;

import za.ac.mphahlele.model.Address;
import za.ac.mphahlele.model.Business;
import za.ac.mphahlele.model.ReceiverRevenue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class BusinessBuilder {
    private String applicantIdentityNumber;
    private String applicantSurname;
    private String applicantName;
    private Address applicantAddress;
    private String typeOfBusiness;
    private String place;
    private String standNo;
    private String siteSize;
    private String chairPersonRemarks;
    private ReceiverRevenue receiverRevenue;

    public BusinessBuilder(String applicantSurname, String applicantName, String applicantIdentityNumber,
                         Address applicantAddress, String typeOfBusiness, String place, String standNo, String siteSize,
                         String chairPersonRemarks, ReceiverRevenue receiverRevenue) {
        this.applicantSurname = applicantSurname;
        this.applicantName = applicantName;
        this.applicantIdentityNumber = applicantIdentityNumber;
        this.applicantAddress = applicantAddress;
        this.typeOfBusiness = typeOfBusiness;
        this.place = place;
        this.standNo = standNo;
        this.siteSize = siteSize;
        this.chairPersonRemarks = chairPersonRemarks;
        this.receiverRevenue = receiverRevenue;
    }

    public static Business buildABusiness(String applicantSurname, String applicantName, String applicantIdentityNumber,
                                      Address applicantAddress, String typeOfBusiness, String place, String standNo, String siteSize,
                                      String chairPersonRemarks, ReceiverRevenue receiverRevenue) {
        return new Business(applicantSurname, applicantName, applicantIdentityNumber,
                applicantAddress, typeOfBusiness, place, standNo, siteSize,
                chairPersonRemarks, receiverRevenue);
    }

    public static Business buildABusiness(int i) {
        String applicantSurname = "Doe" + i;
        String applicantName = "John" + i;
        String applicantIdentityNumber = "1234567899102";
        Address applicantAddress = AddressBuilder.buildAnAddress();
        String  typeOfBusiness = "Business Name" + i;
        String place = "place" + i;
        int standNo = i*5;
        int siteSize = i *20;
        String chairPersonRemarks = "Remarks"+i;
        ReceiverRevenue receiverRevenue = ReceiverRevenueBuilder.buidAReceiverRevenue();

        return buildABusiness(applicantName, applicantSurname, applicantIdentityNumber,
                applicantAddress, typeOfBusiness, place, Integer.toString(standNo), Integer.toString(siteSize),
                chairPersonRemarks, receiverRevenue);
    }


    public static Collection<Business> buildBusinesses(int numberOfBusinesses) {
        List<Business> Businesses = new ArrayList<>();
        for(int i=0;i<numberOfBusinesses;i++) {
            Businesses.add(buildABusiness(i));
        }
        return Businesses;
    }
}
