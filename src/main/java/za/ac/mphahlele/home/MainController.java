package za.ac.mphahlele.home;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.ac.mphahlele.user.service.SecurityService;
import za.ac.mphahlele.user.service.UserService;
import za.ac.mphahlele.village.controller.ListVillageController;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


@Controller
public class MainController {
    public static final String HOME = "/";
    public static final String REDIRECT = "redirect:/";
    public static final String LOGIN = HOME + "login";
    public static final String PRELOGIN = "home/prelogin";
    public static final String POSTLOGIN = "home/postlogin";

    @Inject
    private UserService userService;
    private Logger logger = Logger.getLogger(MainController.class.getName());

    @RequestMapping(value = HOME, method = RequestMethod.GET)
    public String index() {
        if (userService.isAuthenitcated()) {
            if (userService.hasAuthority(SecurityService.ROLE_ADMIN)) {
                return REDIRECT + ListVillageController.VILLAGE_LIST;
            }
            return POSTLOGIN;
        }
        return PRELOGIN;
    }

    @RequestMapping(value = LOGIN)
    public String getSignIn() {
        return PRELOGIN;
    }

    public static List<String> getGenderList() {
        List<String> genderList = new ArrayList<>();
        genderList.add("Male");
        genderList.add("Female");

        return genderList;
    }
}
