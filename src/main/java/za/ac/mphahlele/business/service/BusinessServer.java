package za.ac.mphahlele.business.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.ac.mphahlele.data.repository.BusinessRepository;
import za.ac.mphahlele.exception.EntityExistsException;
import za.ac.mphahlele.model.Business;

import javax.inject.Inject;
import java.util.Collection;


@Service
public class BusinessServer {
    private BusinessRepository businessRepository;

    @Inject
    public BusinessServer(BusinessRepository businessRepository) {
        this.businessRepository = businessRepository;
    }

    @Transactional
    public void create(Business business) {

        throwsExceptionIfBusinessExists(business);
        save(business);
    }

    public void throwsExceptionIfBusinessExists(Business business){
        Business existingBusiness = getBusiness(business.getApplicantIdentityNumber());
        if(existingBusiness != null){
            throw new EntityExistsException("Business with applicant Identity Number: " + business.getApplicantIdentityNumber() +" already exists");
        }
    }

    public void save(Business business){businessRepository.save(business);}
    public Business getBusiness(String applicantIdentityNumber) { return businessRepository.findByApplicantIdentityNumber(applicantIdentityNumber); }
    public Collection<Business> getAll() {return businessRepository.findAll(); }
    public void update(Business business){save(business);}
}
