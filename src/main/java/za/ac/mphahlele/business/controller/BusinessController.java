package za.ac.mphahlele.business.controller;

import org.springframework.ui.Model;
import za.ac.mphahlele.business.service.BusinessServer;
import za.ac.mphahlele.model.Business;

import javax.inject.Inject;

public class BusinessController {
    public static final String BUSINESS = "business";
    public static final String NAME = "name";
    public static final String APPLICANTID = "applicantIdentityNumber";

    @Inject
    protected BusinessServer businessServer;

    public void addAttributes(Model model, Business business){
        model.addAttribute(BUSINESS, business);
    }
}
