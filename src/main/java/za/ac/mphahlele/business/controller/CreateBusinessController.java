package za.ac.mphahlele.business.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import za.ac.mphahlele.user.service.SecurityService;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Business;
import za.ac.mphahlele.support.MessageHelper;

import javax.validation.Valid;

@Controller
@Secured({SecurityService.ROLE_ADMIN})
@RequestMapping(value = CreateBusinessController.BUSINESS_CREATE)
public class CreateBusinessController extends BusinessController{
    public static final String BUSINESS_CREATE = "business/create";

    @RequestMapping(method = RequestMethod.GET)
    public String getForm(Model model){
        Business business = new Business();
        return addAttributesAndReturnForm(model, business);
    }

    private String addAttributesAndReturnForm(Model model, Business business){
        addAttributes(model, business);
        return BUSINESS_CREATE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute(BUSINESS) Business business,
                       BindingResult result,
                       Model model,
                       RedirectAttributes ra){
        if(result.hasErrors()){
           return addAttributesAndReturnForm(model, business);
        }
        try{
            businessServer.create(business);
            MessageHelper.addSuccessAttribute(ra, "business.save.success");
            return MainController.REDIRECT +  ListBusinessController.BUSINESS_LIST;
        }
        catch (Exception e){
            result.addError(new ObjectError(BUSINESS, "Error occured while saving business! " + e.getMessage()));
            return addAttributesAndReturnForm(model, business);
        }
    }
}
