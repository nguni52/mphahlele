package za.ac.mphahlele.business.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value=ListBusinessController.BUSINESS_LIST)
public class ListBusinessController extends BusinessController {
    public static final String BUSINESS_LIST = "business/list";
    public static final String BUSINESSES = "businesses";

    @RequestMapping(method = RequestMethod.GET)
    public String getForm(Model model) {
        model.addAttribute(BUSINESSES, businessServer.getAll());
        return BUSINESS_LIST;
    }
}
