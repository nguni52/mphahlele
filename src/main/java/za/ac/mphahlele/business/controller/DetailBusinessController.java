package za.ac.mphahlele.business.controller;


import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.ac.mphahlele.user.service.SecurityService;
import za.ac.mphahlele.model.Business;

@Controller
@Secured(SecurityService.ROLE_USER)
@RequestMapping(DetailBusinessController.BUSINESS_DETAILS_APPLICANTID)
public class DetailBusinessController extends BusinessController {
    public static final String BUSINESS_DETAILS = "business/details";
    public static final String BUSINESS_DETAILS_APPLICANTID = BUSINESS_DETAILS + "/{"+APPLICANTID+"}";


    @RequestMapping(method= RequestMethod.GET)
    public String getDetails(@PathVariable(APPLICANTID) String applicantIdentityNumber, Model model){
        Business business = businessServer.getBusiness(applicantIdentityNumber);
        addAttributes(model, business);
        return BUSINESS_DETAILS;
    }
}
