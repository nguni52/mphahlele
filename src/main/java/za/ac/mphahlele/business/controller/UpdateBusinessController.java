package za.ac.mphahlele.business.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Business;
import za.ac.mphahlele.support.MessageHelper;

import javax.validation.Valid;

@Controller
@RequestMapping(value = UpdateBusinessController.BUSINESS_UPDATE_ID)
public class UpdateBusinessController extends BusinessController{
    public static final String BUSINESS_UPDATE = "business/update";
    public static final String BUSINESS_UPDATE_ID = BUSINESS_UPDATE + "/{"+APPLICANTID+"}";

    @RequestMapping(method= RequestMethod.GET)
    public String getDetails(@PathVariable(APPLICANTID) String businessID, Model model){
        Business business = businessServer.getBusiness(businessID);
        return addAttributesAndReturnForm(model, business);
    }

    private String addAttributesAndReturnForm(Model model, Business business){
        addAttributes(model, business);
        return BUSINESS_UPDATE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute(BUSINESS) Business business,
                       BindingResult result,
                       Model model,
                       RedirectAttributes ra){
        if(result.hasErrors()){
            return addAttributesAndReturnForm(model, business);
        }
        try{
            businessServer.update(business);
            MessageHelper.addSuccessAttribute(ra, "business.update.success");
            return MainController.REDIRECT +  ListBusinessController.BUSINESS_LIST;
        }
        catch (Exception e){
            result.addError(new ObjectError(BUSINESS, "Error occured while saving business! " + e.getMessage()));
            return addAttributesAndReturnForm(model, business);
        }
    }
}

