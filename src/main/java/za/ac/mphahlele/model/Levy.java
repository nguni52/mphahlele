package za.ac.mphahlele.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.format.annotation.DateTimeFormat;

import org.joda.time.DateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Levy {
    @Id
    @NonNull
    private String id;
    @NotBlank(message="Dependant household id number cannot be empty")
    @NonNull
    private String householdIdNumber;
    private String name;
    private Double amount;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private DateTime dateAdded;
}
