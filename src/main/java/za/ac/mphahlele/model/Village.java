package za.ac.mphahlele.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@NoArgsConstructor
@AllArgsConstructor
public class Village {
    @Id
    private String id;
    @NonNull
    @NotBlank(message = "name cannot be empty")
    private String name;
    @NotBlank(message = "headman cannot be empty")
    private String headman;
    private String positionType;
    private String contactDetails;
}
