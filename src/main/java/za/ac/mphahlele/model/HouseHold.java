package za.ac.mphahlele.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@NoArgsConstructor
@AllArgsConstructor
public class HouseHold {
    @Id
    @NonNull
    @NotBlank(message = "Id Number cannot be empty")
    private String idNumber;
    private String village;
    private String title;
    @NotBlank(message = "Surname cannot be empty")
    private String surname;
    @NotBlank(message = "Full name cannot be empty")
    private String fullName;
    private String standNo;
    private Address postalAddress;
    private String wardNumber;
    private boolean isAccessToElectricity;
    private boolean isAccessToWater;
    private double levyAmount;
}
