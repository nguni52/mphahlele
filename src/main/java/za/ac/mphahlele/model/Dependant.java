package za.ac.mphahlele.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Dependant {
    @Id
    @NonNull
    @NotBlank(message="Dependant ID number cannot be empty")
    private String idNumber;
    @NotBlank(message="Household ID number cannot be empty")
    private String householdIdNumber;
    private String surname;
    private String name;
    @NotBlank(message = "Gender cannot be empty")
    private String gender;
    private String occupation;
    private String relationshipToOwner;
}
