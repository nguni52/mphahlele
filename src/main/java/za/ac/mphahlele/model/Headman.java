package za.ac.mphahlele.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@AllArgsConstructor
@NoArgsConstructor
public class Headman {
    @Id
    @NonNull
    @NotBlank(message="ID Number should not be empty")
    private String idNumber;
    @NotBlank(message="Name cannot be empty")
    private String name;
    @NotBlank(message="Gender cannot be empty")
    private String gender;
    private String unitNumber;
    private String wardNumber;
    private boolean isAccessToElectricity;
    private boolean isAccessToWater;
    private Address postalAddress;
}
