package za.ac.mphahlele.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@NoArgsConstructor
@AllArgsConstructor
public class Church {
    @Id
    @NonNull
    @NotBlank(message="Applicant ID Number cannot be empty")
    private String applicantIdentityNumber;
    private String applicantSurname;
    private String applicantName;
    private Address applicantAddress;
    private String nameOfChurch;
    private String place;
    private String standNo;
    private String siteSize;
    private String chairPersonRemarks;
    private ReceiverRevenue receiverRevenue;
}