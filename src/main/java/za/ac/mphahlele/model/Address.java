package za.ac.mphahlele.model;

import lombok.Data;

@Data
public class Address {
    private String street;
    private String suburb;
    private String city;
    private String postalCode;
}
