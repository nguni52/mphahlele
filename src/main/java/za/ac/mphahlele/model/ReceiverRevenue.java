package za.ac.mphahlele.model;

import lombok.Data;

@Data
public class ReceiverRevenue {
    private String name;
    private String receiptNo;
    private String signature;
    private String date;
}