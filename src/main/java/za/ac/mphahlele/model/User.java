package za.ac.mphahlele.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
public class User {
    @Id
    private String username;
    @NonNull
    @NotBlank(message = "first name cannot be empty")
    private String firstName;
    @NotBlank(message = "first name cannot be empty")
    private String lastName;
    @NotBlank(message = "first name cannot be empty")
    @Pattern(regexp = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{5,30})",
            message = "Reason:" +
                    "\n1.Password should be 5 to 30 characters long." +
                    "\n2.Password must contain at least one digit." +
                    "\n3.Password must contain at least one lowercase letter." +
                    "\n4.Password must contain at least one uppercase letter.")
    private String password;
    private String[] role = {"ROLE_USER"};
}
