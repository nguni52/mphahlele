package za.ac.mphahlele.headman.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.ac.mphahlele.data.repository.HeadmanRepository;
import za.ac.mphahlele.exception.EntityExistsException;
import za.ac.mphahlele.model.Headman;

import javax.inject.Inject;
import java.util.Collection;

@Service
public class HeadmanServer {
    private HeadmanRepository headmanRepository;

    @Inject
    public HeadmanServer(HeadmanRepository headmanRepository) {
        this.headmanRepository = headmanRepository;
    }

    @Transactional
    public void create(Headman headman) {
        throwExceptionIfHeadmanExists(headman);
        save(headman);
    }

    public void save(Headman headman) {
        headmanRepository.save(headman);
    }

    public Headman getHeadman(String idNumber) {
        return headmanRepository.findByIdNumber(idNumber);
    }

    public void throwExceptionIfHeadmanExists(Headman headman) {
        Headman existingHeadman = getHeadman(headman.getIdNumber());
        if(existingHeadman!=null) {
            throw new EntityExistsException("Headman with id: " + headman.getIdNumber() +
                    " already exists.");
        }
    }

    public Collection<Headman> getAll() {
        return headmanRepository.findAll();
    }

    public void update(Headman headman) {
        save(headman);
    }
}
