package za.ac.mphahlele.headman.controller;

import org.apache.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Headman;
import za.ac.mphahlele.support.MessageHelper;
import za.ac.mphahlele.user.service.SecurityService;

import javax.validation.Valid;

/**
 * Created by nguni52 on 14/12/26.
 */
@Controller
@Secured({SecurityService.ROLE_ADMIN, SecurityService.ROLE_USER})
@RequestMapping(value = UpdateHeadmanController.HEADMAN_UPDATE_ID)
public class UpdateHeadmanController extends HeadmanController {
    public static final String HEADMAN_UPDATE = "headman/update";
    public static final String HEADMAN_UPDATE_ID = HEADMAN_UPDATE + MainController.HOME + "{"+ID_NUMBER+"}";
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @RequestMapping
    public String getForm(@PathVariable(ID_NUMBER) String idNumber, Model model) {
        logger.info("getting a headman");
        Headman headman = headmanServer.getHeadman(idNumber);
        logger.info("HEADMAN: " + headman.toString());
        model.addAttribute("genders", MainController.getGenderList());
        return addAttributesAndReturnForm(model, headman);
    }

    private String addAttributesAndReturnForm(Model model, Headman headman) {
        addAttributes(model, headman);
        return HEADMAN_UPDATE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute(HEADMAN) Headman headman,
                       BindingResult result,
                       Model model,
                       RedirectAttributes ra) {
        if(result.hasErrors()) {
            return addAttributesAndReturnForm(model, headman);
        }
        try {
            headmanServer.update(headman);logger.info("UPDATING HEADMAN: " + headman);
            MessageHelper.addSuccessAttribute(ra, "headman.update.success");
            logger.info("Successful save for village");
            return MainController.REDIRECT + ListHeadmanController.HEADMAN_LIST;
        } catch(Exception e) {
            result.addError(new ObjectError(HEADMAN, "Error occurred while updating headman! " + e.getMessage()));
            return addAttributesAndReturnForm(model, headman);
        }
    }
}
