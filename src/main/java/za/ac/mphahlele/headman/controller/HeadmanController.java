package za.ac.mphahlele.headman.controller;

import org.springframework.ui.Model;
import za.ac.mphahlele.headman.service.HeadmanServer;
import za.ac.mphahlele.model.Headman;

import javax.inject.Inject;

/**
 * Created by nguni52 on 2014/07/16.
 */
public class HeadmanController {
    public static final String HEADMAN = "headman";
    public static final String ID_NUMBER = "idNumber";

    @Inject
    protected HeadmanServer headmanServer;

    public void addAttributes(Model model, Headman headman) {
        model.addAttribute(HEADMAN, headman);
    }
}
