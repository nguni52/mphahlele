package za.ac.mphahlele.headman.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Headman;
import za.ac.mphahlele.support.MessageHelper;

import javax.validation.Valid;

@Controller
@RequestMapping(value = CreateHeadmanController.HEADMAN_CREATE)
public class CreateHeadmanController extends HeadmanController {
    public static final String HEADMAN_CREATE = "headman/create";

    @RequestMapping(method = RequestMethod.GET)
    public String getForm(Model model) {
        Headman headman = new Headman();
        model.addAttribute("genders", MainController.getGenderList());
        return addAttributesAndReturnForm(model, headman);
    }

    private String addAttributesAndReturnForm(Model model, Headman headman) {
        addAttributes(model, headman);
        return HEADMAN_CREATE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute(HEADMAN) Headman headman,
                       BindingResult result,
                       Model model,
                       RedirectAttributes ra) {
        if(result.hasErrors()) {
            return addAttributesAndReturnForm(model, headman);
        }
        try {
            headmanServer.create(headman);
            MessageHelper.addSuccessAttribute(ra, "headman.save.success");
            return MainController.REDIRECT +  ListHeadmanController.HEADMAN_LIST;
        } catch(Exception e) {
            result.addError(new ObjectError(HEADMAN, "Error occured while saving headman! " + e.getMessage()));
            return addAttributesAndReturnForm(model, headman);
        }
    }
}
