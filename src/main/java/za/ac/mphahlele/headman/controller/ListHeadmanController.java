package za.ac.mphahlele.headman.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value= ListHeadmanController.HEADMAN_LIST)
public class ListHeadmanController extends HeadmanController {
    public static final String HEADMAN_LIST = "headman/list";
    public static final String HEADMEN = "headmen";

    @RequestMapping(method = RequestMethod.GET)
    public String getForm(Model model) {
        model.addAttribute(HEADMEN, headmanServer.getAll());
        return HEADMAN_LIST;
    }
}
