package za.ac.mphahlele.headman.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.ac.mphahlele.model.Headman;

@Controller
@RequestMapping(value=DetailHeadmanController.HEADMAN_DETAILS_ID_NUMBER)
public class DetailHeadmanController extends HeadmanController {
    public static final String HEADMAN_DETAILS = "headman/details";
    public static final String HEADMAN_DETAILS_ID_NUMBER = HEADMAN_DETAILS + "/{" + ID_NUMBER + "}";

    @RequestMapping(method = RequestMethod.GET)
    public String getDetails(@PathVariable(ID_NUMBER) String idNumber, Model model) {
        Headman headman = getHeadman(idNumber);
        addAttributes(model, headman);
        return HEADMAN_DETAILS;
    }

    private Headman getHeadman(String idNumber) {
        return  headmanServer.getHeadman(idNumber);
    }
}
