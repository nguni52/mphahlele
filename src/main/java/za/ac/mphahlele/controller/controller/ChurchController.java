package za.ac.mphahlele.church.controller;

import org.springframework.ui.Model;
import za.ac.mphahlele.church.service.ChurchServer;
import za.ac.mphahlele.model.Church;

import javax.inject.Inject;

/**
 * Created by nguni52 on 2014/07/17.
 */
public class ChurchController {
    public static final String CHURCH = "church";
    public static final String APPLICANTID = "applicantIdentityNumber";

    @Inject
    protected ChurchServer churchServer;

    public void addAttributes(Model model, Church church) {
        model.addAttribute(CHURCH, church);
    }
}

