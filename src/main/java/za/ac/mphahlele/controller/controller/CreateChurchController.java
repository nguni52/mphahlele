package za.ac.mphahlele.church.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Church;
import za.ac.mphahlele.support.MessageHelper;

import javax.validation.Valid;

@Controller
@RequestMapping(value=CreateChurchController.CHURCH_CREATE)
public class CreateChurchController extends ChurchController {
    public static final String CHURCH_CREATE = "church/create";

    @RequestMapping(method = RequestMethod.GET)
    public String getForm(Model model) {
        Church church = new Church();
        return addAttributesAndReturnForm(model, church);
    }

    private String addAttributesAndReturnForm(Model model, Church church) {
        addAttributes(model, church);
        return CHURCH_CREATE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute(CHURCH) Church church,
                       BindingResult result,
                       Model model,
                       RedirectAttributes ra) {
        if (result.hasErrors()) {
            return addAttributesAndReturnForm(model, church);
        }
        try {
            churchServer.create(church);
            MessageHelper.addSuccessAttribute(ra, "church.save.success");
            return MainController.REDIRECT +  ListChurchController.CHURCH_LIST;
        } catch (Exception e) {
            result.addError(new ObjectError(CHURCH, "Error occurred while saving church! " + e.getMessage()));
            return addAttributesAndReturnForm(model, church);
        }
    }
}
