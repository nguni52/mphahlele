package za.ac.mphahlele.church.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import za.ac.mphahlele.model.Church;

@Controller
@RequestMapping(DetailChurchController.CHURCH_DETAILS_APPLICANTID)
public class DetailChurchController extends ChurchController {
    public static final String CHURCH_DETAILS = "church/details";
    public static final String CHURCH_DETAILS_APPLICANTID = CHURCH_DETAILS + "/{"+APPLICANTID+"}";

    @RequestMapping(method = RequestMethod.GET)
    public String getDetails(@PathVariable(APPLICANTID) String applicantIdentityNumber, Model model) {
        Church church = churchServer.getChurch(applicantIdentityNumber);
        addAttributes(model, church);
        return CHURCH_DETAILS;
    }
}
