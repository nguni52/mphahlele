package za.ac.mphahlele.church.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value=ListChurchController.CHURCH_LIST)
public class ListChurchController extends ChurchController {
    public static final String CHURCH_LIST = "church/list";
    public static final String CHURCHES = "churches";

    @RequestMapping(method = RequestMethod.GET)
    public String getForm(Model model) {
        model.addAttribute(CHURCHES, churchServer.getAll());
        return CHURCH_LIST;
    }
}
