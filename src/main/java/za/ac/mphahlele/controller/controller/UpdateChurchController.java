package za.ac.mphahlele.church.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Church;
import za.ac.mphahlele.support.MessageHelper;

import javax.validation.Valid;

@Controller
@RequestMapping(value=UpdateChurchController.CHURCH_UPDATE_ID)
public class UpdateChurchController extends ChurchController {
    public static final String CHURCH_UPDATE = "church/update";
    public static final String CHURCH_UPDATE_ID = CHURCH_UPDATE + "/{" + APPLICANTID + "}";

    @RequestMapping(method = RequestMethod.GET)
    public String getForm(@PathVariable(APPLICANTID) String churchId, Model model) {
        Church church = churchServer.getChurch(churchId);
        return addAttributesAndReturnForm(model, church);
    }

    private String addAttributesAndReturnForm(Model model, Church church) {
        addAttributes(model, church);
        return CHURCH_UPDATE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute(CHURCH) Church church,
                       BindingResult result,
                       Model model,
                       RedirectAttributes ra) {
        if(result.hasErrors()) {
            return addAttributesAndReturnForm(model, church);
        }
        try {
            churchServer.update(church);
            MessageHelper.addSuccessAttribute(ra, "church.update.success");
            return MainController.REDIRECT +  ListChurchController.CHURCH_LIST;
        } catch(Exception e) {
            result.addError(new ObjectError(CHURCH, "Error occurred while updating church! " + e.getMessage()));
            return addAttributesAndReturnForm(model, church);
        }
    }
}
