package za.ac.mphahlele.church.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.ac.mphahlele.data.repository.ChurchRepository;
import za.ac.mphahlele.exception.EntityExistsException;
import za.ac.mphahlele.model.Church;

import javax.inject.Inject;
import java.util.Collection;

@Service
public class ChurchServer {
    private ChurchRepository churchRepository;

    @Inject
    public ChurchServer(ChurchRepository churchRepository) {
        this.churchRepository = churchRepository;
    }

    @Transactional
    public void create(Church church) {
        throwExceptionIfChurchExists(church);
        save(church);
    }

    public void save(Church church) {
        churchRepository.save(church);
    }

    public void throwExceptionIfChurchExists(Church church) {
        Church existingChurch = getChurch(church.getApplicantIdentityNumber());
        if(existingChurch != null) {
            throw new EntityExistsException("Church with applicant id: " + church.getApplicantIdentityNumber() +
                    " already exists.");
        }
    }

    public Church getChurch(String applicantIdNumber) {
        return churchRepository.findByApplicantIdentityNumber(applicantIdNumber);
    }

    public Collection<Church> getAll() {
        return churchRepository.findAll();
    }

    public void update(Church church) {
        save(church);
    }
}
