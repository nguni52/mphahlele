package za.ac.mphahlele.util;

import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import za.ac.mphahlele.model.Address;

public class RequestUtils {
    public static void addPostalAddress(MockHttpServletRequestBuilder requestBuilder, Address postalAddress) {
        addAddress(requestBuilder, "postalAddress", postalAddress);
    }

    public static void addAddress(MockHttpServletRequestBuilder requestBuilder,
                                  String addressFieldName,
                                  Address postalAddress) {
        requestBuilder
                .param(addressFieldName + ".street", postalAddress.getStreet())
                .param(addressFieldName + ".suburb", postalAddress.getSuburb())
                .param(addressFieldName + ".city", postalAddress.getCity())
                .param(addressFieldName + ".postalCode", postalAddress.getPostalCode());
    }
}
