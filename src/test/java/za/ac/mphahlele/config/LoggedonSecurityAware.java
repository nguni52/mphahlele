package za.ac.mphahlele.config;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import za.ac.mphahlele.builder.UserBuilder;
import za.ac.mphahlele.data.MongoConfig;
import za.ac.mphahlele.data.repository.UserRepository;
import za.ac.mphahlele.user.service.SecurityService;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by nguni52 on 2014/08/08.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
@WebAppConfiguration
@ContextConfiguration(classes = {AppConfig.class, SecurityConfig.class, WebMVCConfig.class,
        MongoConfig.class,})
public abstract class LoggedonSecurityAware extends WebSecurityConfigurationAware {


    @Inject
    private UserRepository userAccountRepository;

    @After
    public void tearDown() throws Exception {
        userAccountRepository.deleteAll();
    }


    @Before
    public void logon() throws Exception {
        za.ac.mphahlele.model.User user = UserBuilder.buildAUser(1);

        List<GrantedAuthority> authorityList = AuthorityUtils.createAuthorityList(SecurityService.ROLE_ADMIN);
        User springUser = new User(user.getUsername(), user.getPassword(),
                authorityList);
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(springUser, null, authorityList);
        testingAuthenticationToken.setAuthenticated(false);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
    }
}

