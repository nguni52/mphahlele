package za.ac.mphahlele.config;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import za.ac.mphahlele.builder.UserBuilder;
import za.ac.mphahlele.data.MongoConfig;
import za.ac.mphahlele.data.repository.RepositoryTest;
import za.ac.mphahlele.user.service.SecurityService;

import javax.inject.Inject;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ActiveProfiles("test")
@ContextConfiguration(classes = {AppConfig.class, MongoConfig.class, SecurityConfig.class, WebMVCConfig.class})
public class WebAppConfigurationAware extends RepositoryTest {
    @Inject
    protected WebApplicationContext wac;
    protected MockMvc mockMvc;

    @Before
    public void before() {
        this.mockMvc = webAppContextSetup(this.wac).build();
    }

    @Before
    public void logon() throws Exception {
        za.ac.mphahlele.model.User user = UserBuilder.buildAUser(1);

        User springUser = new User(user.getUsername(), user.getPassword(),
                AuthorityUtils.createAuthorityList(SecurityService.ROLE_USER));
        TestingAuthenticationToken testingAuthenticationToken = new TestingAuthenticationToken(springUser, null);
        SecurityContextHolder.getContext().setAuthentication(testingAuthenticationToken);
    }
}
