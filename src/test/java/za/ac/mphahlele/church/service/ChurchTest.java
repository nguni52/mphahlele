package za.ac.mphahlele.church.service;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.mockito.Mock;
import za.ac.mphahlele.builder.ChurchBuilder;
import za.ac.mphahlele.data.MockitoTest;
import za.ac.mphahlele.data.repository.ChurchRepository;
import za.ac.mphahlele.exception.EntityExistsException;
import za.ac.mphahlele.model.Church;

import static org.mockito.Mockito.*;

public class ChurchTest extends MockitoTest {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    @Mock
    private ChurchRepository churchRepository;

    @Test(expected = EntityExistsException.class)
    public void throwExceptionIfChurchWithSameApplicantIdExists() {
        Church church = ChurchBuilder.buildAChurch(1);
        when(churchRepository.findByApplicantIdentityNumber(church.getApplicantIdentityNumber())).thenReturn(church);
        ChurchServer churchServer = new ChurchServer(churchRepository);
        churchServer.create(church);
        verify(churchRepository, never()).save(church);
    }
}
