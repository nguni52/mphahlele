package za.ac.mphahlele.church.controller;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import za.ac.mphahlele.builder.ChurchBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.ChurchRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Church;

import javax.inject.Inject;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class CreateChurchControllerTest extends WebAppConfigurationAware {
    @Inject
    private ChurchRepository churchRepository;
    private MockHttpServletRequestBuilder requestBuilder;
    private Church church;

    @Before
    public void setUp() throws Exception {
        church = ChurchBuilder.buildAChurch(1);
        requestBuilder = post(MainController.HOME + CreateChurchController.CHURCH_CREATE);
    }

    @After
    public void tearDown() {
        churchRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        ResultActions result = mockMvc.perform(get(MainController.HOME +
        CreateChurchController.CHURCH_CREATE));
        assertAddFormDisplayed(result);
    }

    private void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(view().name(CreateChurchController.CHURCH_CREATE));
    }
}
