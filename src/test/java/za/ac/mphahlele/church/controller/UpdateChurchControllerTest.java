package za.ac.mphahlele.church.controller;

import org.apache.log4j.Logger;
import org.hamcrest.core.IsEqual;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import za.ac.mphahlele.builder.AddressBuilder;
import za.ac.mphahlele.builder.ChurchBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.ChurchRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Address;
import za.ac.mphahlele.model.Church;
import za.ac.mphahlele.util.RequestUtils;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class UpdateChurchControllerTest extends WebAppConfigurationAware {
    @Inject
    private ChurchRepository churchRepository;
    private MockHttpServletRequestBuilder requestBuilder;
    private Church church;
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Before
    public void setUp() throws Exception {
        church = ChurchBuilder.buildAChurch(1);
        churchRepository.save(church);
        requestBuilder = post(MainController.HOME +  UpdateChurchController.CHURCH_UPDATE
                + MainController.HOME + church.getApplicantIdentityNumber());
    }

    @After
    public void tearDown() throws Exception {
        churchRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        String updateChurchRequest = MainController.HOME +
                UpdateChurchController.CHURCH_UPDATE + MainController.HOME + church.getApplicantIdentityNumber();
        logger.info(updateChurchRequest);
        ResultActions result = mockMvc.perform(get(updateChurchRequest));
        assertAddFormDisplayed(result);
    }

    private void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(model().attribute(ChurchController.CHURCH,
                hasProperty("applicantIdentityNumber", IsEqual.equalTo(church.getApplicantIdentityNumber()))))
                .andExpect(view().name(UpdateChurchController.CHURCH_UPDATE));
    }

    @Test
    public void savesChurch() throws Exception {
        String expectedNameOfChurch = "REMA Church Randburg";
        String expectedPlace = "Randburg";
        Church church1 = church;
        church1.setNameOfChurch(expectedNameOfChurch);
        church1.setPlace(expectedPlace);
        addChurch(requestBuilder, church1);
        mockMvc.perform(requestBuilder);

        List<Church> churches = churchRepository.findAll();
        assertEquals(1, churches.size());
        assertEquals(expectedNameOfChurch, churches.get(0).getNameOfChurch());
        assertEquals(expectedPlace, churches.get(0).getPlace());
    }

    @Test
    public void catchException() throws Exception {
        church.setApplicantIdentityNumber("");
        churchRepository.save(church);
        addChurch(requestBuilder, church);
        ResultActions result = mockMvc.perform(requestBuilder);
        result.andExpect(model().hasErrors());
        assertAddFormDisplayed(result);
    }

    private void addChurch(MockHttpServletRequestBuilder requestBuilder, Church church1) {
        Address postalAddress = AddressBuilder.buildAnAddress();
        requestBuilder.param("applicantIdentityNumber", church.getApplicantIdentityNumber())
                .param("applicantSurname", church.getApplicantSurname())
                .param("applicantName", church.getApplicantName())
                .param("nameOfChurch", church.getNameOfChurch())
                .param("place", church.getPlace())
                .param("standNo", church.getStandNo())
                .param("siteSize", church.getSiteSize())
                .param("chairPersonRemarks", church.getChairPersonRemarks());
        RequestUtils.addPostalAddress(requestBuilder, postalAddress);
    }
}