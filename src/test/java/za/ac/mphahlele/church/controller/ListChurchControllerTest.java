package za.ac.mphahlele.church.controller;

import org.junit.After;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import za.ac.mphahlele.builder.ChurchBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.ChurchRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Church;

import javax.inject.Inject;
import java.util.Collection;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class ListChurchControllerTest extends WebAppConfigurationAware {
    @Inject
    private ChurchRepository churchRepository;

    @After
    public void tearDown() {
        churchRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        int numberOfChurches = 10;
        Collection<Church> churches = ChurchBuilder.buildChurches(numberOfChurches);
        churchRepository.save(churches);
        ResultActions result = mockMvc.perform(get(MainController.HOME +
            ListChurchController.CHURCH_LIST));
        result.andExpect(model().attribute(ListChurchController.CHURCHES, hasSize(numberOfChurches)));
        result.andExpect(view().name(ListChurchController.CHURCH_LIST));
    }
}
