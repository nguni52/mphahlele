package za.ac.mphahlele.church.controller;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import za.ac.mphahlele.builder.ChurchBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.ChurchRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Church;

import javax.inject.Inject;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class DetailChurchControllerTest extends WebAppConfigurationAware {
    private Church church;

    @Inject
    private ChurchRepository churchRepository;

    @Before
    public void setUp() throws Exception {
        church = ChurchBuilder.buildAChurch(1);
        churchRepository.save(church);
    }

    @After
    public void tearDown() {
        churchRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribuet() throws Exception {
        ResultActions result = mockMvc.perform(get(MainController.HOME +
        DetailChurchController.CHURCH_DETAILS + MainController.HOME + church.getApplicantIdentityNumber()));
        assertAddFormDisplayed(result);
    }

    public void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(model().attribute(DetailChurchController.CHURCH, church))
                .andExpect(view().name(DetailChurchController.CHURCH_DETAILS));
    }
}
