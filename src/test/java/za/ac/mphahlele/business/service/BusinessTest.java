package za.ac.mphahlele.business.service;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.mockito.Mock;
import za.ac.mphahlele.builder.BusinessBuilder;
import za.ac.mphahlele.data.MockitoTest;
import za.ac.mphahlele.data.repository.BusinessRepository;
import za.ac.mphahlele.exception.EntityExistsException;
import za.ac.mphahlele.model.Business;
import static org.mockito.Mockito.*;


public class BusinessTest extends MockitoTest {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    @Mock
    private BusinessRepository businessRepository;

    @Test(expected = EntityExistsException.class)
    public void throwExceptionIfBusinessWithSameNameExists(){
        Business business = BusinessBuilder.buildABusiness(1);
        when(businessRepository.findByApplicantIdentityNumber(business.getApplicantIdentityNumber())).thenReturn(business);
        BusinessServer businessServer = new BusinessServer(businessRepository);
        businessServer.create(business);
        verify(businessRepository, never()).save(business);
    }

}
