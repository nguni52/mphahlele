package za.ac.mphahlele.business.controller;

import org.hamcrest.core.IsEqual;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import za.ac.mphahlele.builder.AddressBuilder;
import za.ac.mphahlele.builder.BusinessBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.BusinessRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Address;
import za.ac.mphahlele.model.Business;
import za.ac.mphahlele.util.RequestUtils;


import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class UpdateBusinessControllerTest extends WebAppConfigurationAware{
    @Inject
    private BusinessRepository businessRepository;
    private MockHttpServletRequestBuilder requestBuilder;
    private Business business;
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Before
    public void setUp() throws Exception{
        business = BusinessBuilder.buildABusiness(1);
        businessRepository.save(business);
        requestBuilder = post(MainController.HOME +  UpdateBusinessController.BUSINESS_UPDATE
                + MainController.HOME + business.getApplicantIdentityNumber());

    }
    @After
    public void tearDown(){ businessRepository.deleteAll();}

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception{
        String updateBusinessRequest = MainController.HOME +
                UpdateBusinessController.BUSINESS_UPDATE + MainController.HOME + business.getApplicantIdentityNumber();
        logger.info(updateBusinessRequest);
        ResultActions result = mockMvc.perform(get(updateBusinessRequest));
        assertAddFormDisplayed(result);
    }

    private void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(model().attribute(BusinessController.BUSINESS,
                hasProperty("applicantIdentityNumber", IsEqual.equalTo(business.getApplicantIdentityNumber()))))
                .andExpect(view().name(UpdateBusinessController.BUSINESS_UPDATE));
    }

    @Test
    public void savesBusiness() throws Exception {
        String expectedTypeOfBusiness = "DSQUARED CLOTHING";
        String expectedPlace = "Fourways";
        Business business1 = new Business();
        business1.setTypeOfBusiness(expectedTypeOfBusiness);
        business1.setPlace(expectedPlace);
        addBusiness(requestBuilder, business1);
        mockMvc.perform(requestBuilder);

        List<Business> businesses = businessRepository.findAll();
        assertEquals(1, businesses.size());
    }

    private void addBusiness(MockHttpServletRequestBuilder requestBuilder, Business business1) {
        Address postalAddress = AddressBuilder.buildAnAddress();
        requestBuilder.param("applicantIdentityNumber", business.getApplicantIdentityNumber())
                .param("applicantSurname", business.getApplicantSurname())
                .param("applicantName", business.getApplicantName())
                .param("typeOfBusiness", business.getTypeOfBusiness())
                .param("place", business.getPlace())
                .param("standNo", business.getStandNo())
                .param("siteSize", business.getSiteSize())
                .param("chairPersonRemarks", business.getChairPersonRemarks());
        RequestUtils.addPostalAddress(requestBuilder, postalAddress);
    }

}
