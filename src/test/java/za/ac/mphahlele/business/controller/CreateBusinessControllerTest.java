package za.ac.mphahlele.business.controller;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import za.ac.mphahlele.builder.BusinessBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.BusinessRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Business;

import javax.inject.Inject;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class CreateBusinessControllerTest extends WebAppConfigurationAware{
    @Inject
    private BusinessRepository businessRepository;
    private MockHttpServletRequestBuilder requestBuilder;
    private Business business;

    @Before
    public void setUp() throws Exception{
        business = BusinessBuilder.buildABusiness(1);
        requestBuilder = post(MainController.HOME + CreateBusinessController.BUSINESS_CREATE);

    }
    @After
    public void tearDown(){ businessRepository.deleteAll();}

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception{
        ResultActions result = mockMvc.perform(get(MainController.HOME +
        CreateBusinessController.BUSINESS_CREATE));
        assertAddFormDisplayed(result);
    }

    private void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(view().name(CreateBusinessController.BUSINESS_CREATE));
    }
}
