package za.ac.mphahlele.business.controller;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import za.ac.mphahlele.builder.BusinessBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.BusinessRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Business;

import javax.inject.Inject;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class DetailBusinessControllerTest extends WebAppConfigurationAware {
        private Logger logger = Logger.getLogger(this.getClass().getName());
        private Business business;

        @Inject
        private BusinessRepository businessRepository;

        @Before
        public void setUp() throws Exception {
            business = BusinessBuilder.buildABusiness(1);
            businessRepository.save(business);

        }

        @After
        public void tearDown(){
            businessRepository.deleteAll();
        }

        @Test
        public void getCorrectViewAndModelAttribute() throws Exception {
            ResultActions result = mockMvc.perform((get(MainController.HOME +
                    DetailBusinessController.BUSINESS_DETAILS + MainController.HOME + business.getApplicantIdentityNumber())));
            assertAddFormDisplayed(result);
        }

        public void assertAddFormDisplayed(ResultActions result) throws Exception {
            result.andExpect(model().attribute(DetailBusinessController.BUSINESS, business))
                    .andExpect(view().name(DetailBusinessController.BUSINESS_DETAILS));
        }
}
