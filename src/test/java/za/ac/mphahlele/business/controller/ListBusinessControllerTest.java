package za.ac.mphahlele.business.controller;


import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import za.ac.mphahlele.builder.BusinessBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.BusinessRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Business;

import javax.inject.Inject;
import java.util.Collection;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class ListBusinessControllerTest extends WebAppConfigurationAware{
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Inject
    private BusinessRepository businessRepository;

    @After
    public void tearDown() {
        businessRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception{
        int numberOfBusinesses = 10;
        Collection<Business> businesses = BusinessBuilder.buildBusinesses(numberOfBusinesses);
        businessRepository.save(businesses);
        ResultActions result = mockMvc.perform(get(MainController.HOME +
        ListBusinessController.BUSINESS_LIST));
        result.andExpect(model().attribute(ListBusinessController.BUSINESSES, hasSize(numberOfBusinesses)));
        result.andExpect(view().name(ListBusinessController.BUSINESS_LIST));

    }


}
