package za.ac.mphahlele.household.dependants.controller;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import za.ac.mphahlele.builder.DependantBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.DependantRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.household.dependant.controller.DetailDependantController;
import za.ac.mphahlele.model.Dependant;

import javax.inject.Inject;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 * Created by nguni52 on 14/12/26.
 */
public class DetailsDependantControllerTest extends WebAppConfigurationAware {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private Dependant dependant;
    private String requestUrl;
    @Inject
    private DependantRepository dependantRepository;


    @Before
    public void setUp() throws Exception {
        dependant = DependantBuilder.buildADependant(1);
        dependantRepository.save(dependant);
        requestUrl = MainController.HOME +
                DetailDependantController.DEPENDANT_DETAILS + MainController.HOME + dependant.getIdNumber();
    }

    @After
    public void tearDown() {
        dependantRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        logger.info(dependant.toString());
        ResultActions result  = mockMvc.perform(get(requestUrl));
        assertAddFormDisplayed(result);
    }

    private void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(model().attribute(DetailDependantController.DEPENDANT, dependant))
                .andExpect(view().name(DetailDependantController.DEPENDANT_DETAILS));
    }
}
