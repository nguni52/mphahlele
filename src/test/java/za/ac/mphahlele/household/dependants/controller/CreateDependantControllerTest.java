package za.ac.mphahlele.household.dependants.controller;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import za.ac.mphahlele.builder.DependantBuilder;
import za.ac.mphahlele.builder.HouseHoldBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.DependantRepository;
import za.ac.mphahlele.data.repository.HouseHoldRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.household.dependant.controller.CreateDependantController;
import za.ac.mphahlele.household.dependant.controller.DependantController;
import za.ac.mphahlele.household.main.controller.DetailHouseHoldController;
import za.ac.mphahlele.model.Dependant;
import za.ac.mphahlele.model.HouseHold;
import za.ac.mphahlele.support.Message;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CreateDependantControllerTest extends WebAppConfigurationAware {
    @Inject
    private DependantRepository dependantRepository;
    @Inject
    private HouseHoldRepository houseHoldRepository;
    private MockHttpServletRequestBuilder requestBuilder;
    private Dependant dependant;
    private HouseHold houseHold;
    private String requestUrl;
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private static final String HOUSEHOLD_ID_NUMBER = "1234561";

    @Before
    public void setUp() throws Exception {
        houseHold = HouseHoldBuilder.buildAHouseHold(1);
        logger.info(houseHold.toString());
        houseHoldRepository.save(houseHold);
        dependant = DependantBuilder.buildADependant(HOUSEHOLD_ID_NUMBER, 1);
        requestUrl = MainController.HOME + CreateDependantController.DEPENDANT_CREATE
                + MainController.HOME + houseHold.getIdNumber();
        requestBuilder = post(requestUrl);
    }

    @After
    public void tearDown() {
        dependantRepository.deleteAll();
        houseHoldRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        logger.info(requestUrl);
        ResultActions result = mockMvc.perform(get(requestUrl));
        assertAddFormDisplayed(result);
    }

    private void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(model().attribute(DependantController.DEPENDANT,
                hasProperty("householdIdNumber", equalTo(HOUSEHOLD_ID_NUMBER))))
                .andExpect(view().name(CreateDependantController.DEPENDANT_CREATE))
                .andDo(print());
    }

    @Test
    public void savesDependant() throws Exception {
        addDependant(requestBuilder, dependant);
        mockMvc.perform(requestBuilder)
                .andExpect(redirectedUrl(MainController.HOME + DetailHouseHoldController.HOUSEHOLD_DETAILS +
                        MainController.HOME + dependant.getHouseholdIdNumber()))
                .andExpect(flash().attribute(Message.MESSAGE_ATTRIBUTE,
                        hasProperty("type", equalTo(Message.Type.SUCCESS))))
        .andDo(print());

        List<Dependant> dependantList = dependantRepository.findAll();
        assertEquals(1, dependantList.size());
        assertEquals(dependant.getIdNumber(), dependantList.get(0).getIdNumber());
        assertEquals(dependant.getName(), dependantList.get(0).getName());
    }

    private void addDependant(MockHttpServletRequestBuilder requestBuilder, Dependant dependant) {
        requestBuilder.param("idNumber", dependant.getIdNumber())
                .param("householdIdNumber", dependant.getHouseholdIdNumber())
                .param("surname", dependant.getSurname())
                .param("name", dependant.getName())
                .param("gender", dependant.getGender())
                .param("occupation", dependant.getOccupation())
                .param("relationshipToOwner", dependant.getRelationshipToOwner());
    }
}
