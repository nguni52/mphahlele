package za.ac.mphahlele.household.dependants.service;

import org.junit.Test;
import org.mockito.Mock;
import za.ac.mphahlele.builder.DependantBuilder;
import za.ac.mphahlele.data.MockitoTest;
import za.ac.mphahlele.data.repository.DependantRepository;
import za.ac.mphahlele.exception.EntityExistsException;
import za.ac.mphahlele.household.dependant.service.DependantServer;
import za.ac.mphahlele.model.Dependant;

import static org.mockito.Mockito.*;

public class DependantTest extends MockitoTest {
    @Mock
    private DependantRepository dependantRepository;

    @Test(expected = EntityExistsException.class)
    public void throwExceptionIfHouseHoldWithSameNameExists() {
        Dependant dependant = DependantBuilder.buildADependant(1);
        when(dependantRepository.findByIdNumber(dependant.getIdNumber())).thenReturn(dependant);
        DependantServer dependantServer = new DependantServer(dependantRepository);
        dependantServer.create(dependant);
        verify(dependantRepository, never()).save(dependant);
    }
}
