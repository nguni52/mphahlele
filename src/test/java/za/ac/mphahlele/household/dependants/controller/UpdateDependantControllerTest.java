package za.ac.mphahlele.household.dependants.controller;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import za.ac.mphahlele.builder.DependantBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.DependantRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.household.dependant.controller.DependantController;
import za.ac.mphahlele.household.dependant.controller.UpdateDependantController;
import za.ac.mphahlele.model.Dependant;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class UpdateDependantControllerTest extends WebAppConfigurationAware {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    private static final String HOUSEHOLD_ID_NUMBER = "1234561";
    @Inject
    DependantRepository dependantRepository;
    private Dependant dependant;
    private MockHttpServletRequestBuilder requestBuilder;
    private String requestUrl;


    @Before
    public void setUp() throws Exception {
        dependant = DependantBuilder.buildADependant(HOUSEHOLD_ID_NUMBER, 1);
        logger.info("\n\n\n\n" + dependant.toString() + "\n\n");
        dependantRepository.save(dependant);
        requestUrl = MainController.HOME + UpdateDependantController.DEPENDANT_UPDATE + MainController.HOME +
                dependant.getIdNumber();
        requestBuilder = post(requestUrl);
    }

    @After
    public void tearDown() {
        dependantRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        ResultActions result = mockMvc.perform(get(requestUrl));
        logger.info(HOUSEHOLD_ID_NUMBER + "\n\n\n");
        assertAddFormDisplayed(result);
    }

    private void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(model().attribute(DependantController.DEPENDANT,
                hasProperty("householdIdNumber", equalTo(HOUSEHOLD_ID_NUMBER))))
                .andExpect(view().name(UpdateDependantController.DEPENDANT_UPDATE))
                .andDo(print());
    }

    @Test
    public void savesDependant() throws Exception {
        addDependant(dependant);
        mockMvc.perform(requestBuilder).andDo(print());

        List<Dependant> dependants = dependantRepository.findAll();
        assertEquals(dependants.size(), 1);
        assertEquals(dependant.getIdNumber(), dependants.get(0).getIdNumber());
        assertEquals(dependant.getGender(), dependants.get(0).getGender());
        assertEquals(dependant.getSurname(), dependants.get(0).getSurname());
    }

    private void addDependant(Dependant dependant) {
        requestBuilder.param("idNumber", dependant.getIdNumber())
                .param("householdIdNumber", dependant.getHouseholdIdNumber())
                .param("surname",dependant.getSurname())
                .param("name", dependant.getName())
                .param("occupation", dependant.getOccupation())
                .param("gender", dependant.getGender())
                .param("relationshipToOwner", dependant.getRelationshipToOwner());
    }
}
