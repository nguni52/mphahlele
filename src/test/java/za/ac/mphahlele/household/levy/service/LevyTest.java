package za.ac.mphahlele.household.levy.service;


import org.junit.Test;
import org.mockito.Mock;
import za.ac.mphahlele.builder.LevyBuilder;
import za.ac.mphahlele.data.MockitoTest;
import za.ac.mphahlele.data.repository.LevyRepository;
import za.ac.mphahlele.exception.EntityExistsException;
import za.ac.mphahlele.model.Levy;

import static org.mockito.Mockito.when;

import static org.mockito.Mockito.*;

public class LevyTest extends MockitoTest {
    @Mock
    private LevyRepository levyRepository;

    @Test(expected = EntityExistsException.class)
    public void throwExceptionIfLevyWithSameNameAndDateExists() {
        Levy levy = LevyBuilder.buildALevy(1);
        when(levyRepository.findById(levy.getId())).thenReturn(levy);
        LevyServer levyServer = new LevyServer(levyRepository);
        levyServer.create(levy);
        verify(levyRepository, never()).save(levy);
    }
}
