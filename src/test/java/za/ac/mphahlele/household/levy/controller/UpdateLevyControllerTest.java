package za.ac.mphahlele.household.levy.controller;

import org.apache.log4j.Logger;
import org.hamcrest.core.IsEqual;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import za.ac.mphahlele.builder.LevyBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.LevyRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Levy;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 * Created by nguni52 on 14/12/26.
 */
public class UpdateLevyControllerTest extends WebAppConfigurationAware {
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Inject
    LevyRepository levyRepository;
    private Levy levy;
    private MockHttpServletRequestBuilder requestBuilder;
    private String levyRequestString;

    @Before
    public void setUp() throws Exception {
        levy = LevyBuilder.buildALevy(1);
        levyRepository.save(levy);
        levyRequestString = MainController.HOME + UpdateLevyController.LEVY_UPDATE +
                MainController.HOME + levy.getId();
        requestBuilder = post(levyRequestString);
    }

    @After
    public void tearDown() {
        levyRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        ResultActions result = mockMvc.perform(get(levyRequestString));
        assertAddFormDisplayed(result);
    }

    private void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(model().attribute(LevyController.LEVY,
                hasProperty("id", IsEqual.equalTo(levy.getId()))))
                .andExpect(view().name(UpdateLevyController.LEVY_UPDATE));
    }

    @Test
    public void savesHouseHold() throws Exception {
        String expectedName = "name1";
        Double expectedAmount = 11.00;
        Levy expectedLevy = levy;
        expectedLevy.setName(expectedName);
        expectedLevy.setAmount(expectedAmount);
        addLevy(requestBuilder, expectedLevy);
        mockMvc.perform(requestBuilder);

        List<Levy> levies = levyRepository.findAll();
        assertEquals(1, levies.size());
        assertEquals(expectedName, levies.get(0).getName());
        assertEquals(expectedAmount, levies.get(0).getAmount());
        logger.info(levies.get(0).toString());
    }

    @Test
    public void catchException() throws Exception {
        levy.setHouseholdIdNumber("");
        levyRepository.save(levy);
        addLevy(requestBuilder, levy);
        ResultActions result = mockMvc.perform(requestBuilder);
        result.andExpect(model().hasErrors());
        assertAddFormDisplayed(result);
    }

    private void addLevy(MockHttpServletRequestBuilder requestBuilder, Levy levy) {
        requestBuilder.param("id", levy.getId())
                .param("houseHoldIdNumber", levy.getHouseholdIdNumber())
                .param("amount", Double.toString(levy.getAmount()))
                .param("dateAdded", levy.getDateAdded().toString());
    }
}
