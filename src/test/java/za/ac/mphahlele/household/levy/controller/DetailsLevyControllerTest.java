package za.ac.mphahlele.household.levy.controller;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import za.ac.mphahlele.builder.LevyBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.LevyRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Levy;

import javax.inject.Inject;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 * Created by nguni52 on 14/12/26.
 */
public class DetailsLevyControllerTest extends WebAppConfigurationAware {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private Levy levy;

    @Inject
    private LevyRepository levyRepository;
    private String levyRequestString;

    @Before
    public void setUp() throws Exception {
        levy = LevyBuilder.buildALevy(1);
        levyRepository.save(levy);
        levyRequestString = MainController.HOME + DetailLevyController.LEVY_DETAILS + MainController.HOME
                + levy.getId();
    }

    @After
    public void tearDown() {
        levyRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        logger.info(levy.toString());
        ResultActions result = mockMvc.perform(get(levyRequestString));
        assertAddFormDisplayed(result);
    }

    public void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(model().attribute(DetailLevyController.LEVY, levy))
                .andExpect(view().name(DetailLevyController.LEVY_DETAILS));
    }
}
