package za.ac.mphahlele.household.levy.controller;

import org.hamcrest.beans.HasPropertyWithValue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import za.ac.mphahlele.builder.LevyBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.LevyRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.household.main.controller.DetailHouseHoldController;
import za.ac.mphahlele.model.Levy;
import za.ac.mphahlele.support.Message;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CreateLevyControllerTest extends WebAppConfigurationAware {
    @Inject
    private LevyRepository levyRepository;
    private MockHttpServletRequestBuilder requestBuilder;
    private Levy levy;
    private String requestUrl;

    @Before
    public void setUp() throws Exception {
        levy = LevyBuilder.buildALevy(1);
        requestUrl = MainController.HOME + CreateLevyController.LEVY_CREATE + MainController.HOME +
                levy.getHouseholdIdNumber();
        requestBuilder = post(requestUrl);
    }

    @After
    public void tearDown() throws Exception {
        levyRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        ResultActions result = mockMvc.perform(get(requestUrl));
        assertAddFormDisplayed(result);
    }

    private void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(view().name(CreateLevyController.LEVY_CREATE));
    }

    //@Test
    public void savesVillage() throws Exception {
        addLevy(requestBuilder, levy);
        mockMvc.perform(requestBuilder)
                .andExpect(redirectedUrl(MainController.HOME + DetailHouseHoldController.HOUSEHOLD_DETAILS))
                .andExpect(flash().attribute(Message.MESSAGE_ATTRIBUTE,
                        HasPropertyWithValue.hasProperty("type", equalTo(Message.Type.SUCCESS))))
        ;

        List<Levy> levyList = levyRepository.findAll();
        assertEquals(1, levyList.size());
        assertEquals(levy.getName(), levyList.get(0).getName());
        assertEquals(levy.getAmount(), levyList.get(0).getAmount());
    }

    private void addLevy(MockHttpServletRequestBuilder requestBuilder, Levy levy) {
        requestBuilder.param("id", levy.getId())
                .param("householdIdNumber", levy.getHouseholdIdNumber())
                .param("name", levy.getName())
                .param("amount", Double.toString(levy.getAmount()))
                .param("dateAdded", levy.getDateAdded().toString());
    }
}
