package za.ac.mphahlele.household.main.controller;

import org.apache.log4j.Logger;
import org.hamcrest.beans.HasPropertyWithValue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import za.ac.mphahlele.builder.HouseHoldBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.HouseHoldRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Address;
import za.ac.mphahlele.model.HouseHold;
import za.ac.mphahlele.support.Message;
import za.ac.mphahlele.util.RequestUtils;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class CreateHouseHoldControllerTest extends WebAppConfigurationAware {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    @Inject
    private HouseHoldRepository houseHoldRepository;
    private MockHttpServletRequestBuilder requestBuilder;
    private HouseHold houseHold;

    @Before
    public void setUp() throws Exception {
        houseHold = HouseHoldBuilder.buildAHouseHold(1);
        requestBuilder = post(MainController.HOME + CreateHouseHoldController.HOUSEHOLD_CREATE);
    }

    @After
    public void tearDown() throws Exception {
        houseHoldRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        ResultActions result = mockMvc.perform(get(MainController.HOME +
                CreateHouseHoldController.HOUSEHOLD_CREATE));
        assertAddFormDisplayed(result);
    }

    private void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(view().name(CreateHouseHoldController.HOUSEHOLD_CREATE));
    }

    @Test
    public void savesHouseHolds() throws Exception {
        logger.info("\n\n\n\n*************" + houseHold.toString() + "*************\n\n\n\n");
        logger.info("\n\n\n\n***************BEGIN savesHouseHolds***************\n\n\n\n");
        addHouseHolds(requestBuilder, houseHold);

        mockMvc.perform(requestBuilder)
                .andExpect(redirectedUrl(MainController.HOME +  ListHouseHoldController.HOUSEHOLD_LIST))
                .andExpect(flash().attribute(Message.MESSAGE_ATTRIBUTE,
                        HasPropertyWithValue.hasProperty("type", equalTo(Message.Type.SUCCESS))));

        List<HouseHold> houseHoldList = houseHoldRepository.findAll();
        assertEquals(1, houseHoldList.size());
        assertEquals(houseHold, houseHoldList.get(0));

        logger.info("\n\n\n\n***************END savesHouseHolds***************\n\n\n\n");
    }

    private void addHouseHolds(MockHttpServletRequestBuilder requestBuilder, HouseHold houseHold) {
        Address postalAddress = houseHold.getPostalAddress();
        requestBuilder.param("idNumber", houseHold.getIdNumber())
                .param("title", houseHold.getTitle())
                .param("surname", houseHold.getSurname())
                .param("fullName", houseHold.getFullName())
                .param("standNo", houseHold.getStandNo())
                .param("wardNumber", houseHold.getWardNumber())
                .param("levyAmount", Double.toString(houseHold.getLevyAmount()));
        RequestUtils.addPostalAddress(requestBuilder, postalAddress);
    }
}
