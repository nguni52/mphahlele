package za.ac.mphahlele.household.main.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import za.ac.mphahlele.builder.DependantBuilder;
import za.ac.mphahlele.builder.HouseHoldBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.DependantRepository;
import za.ac.mphahlele.data.repository.HouseHoldRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Dependant;
import za.ac.mphahlele.model.HouseHold;

import javax.inject.Inject;
import java.util.Collection;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class DetailsHouseHoldControllerTest extends WebAppConfigurationAware {
    private static final int NUMBER_OF_DEPENDANTS = 10;
    private HouseHold houseHold;
    private Collection<Dependant> dependants;

    @Inject
    private HouseHoldRepository houseHoldRepository;
    @Inject
    private DependantRepository dependantRepository;

    @Before
    public void setUp() throws Exception {
        houseHold = HouseHoldBuilder.buildAHouseHold(1);
        houseHoldRepository.save(houseHold);
        dependants = DependantBuilder.buildDependants(houseHold.getIdNumber(), NUMBER_OF_DEPENDANTS);
        dependants = dependantRepository.save(dependants);
    }

    @After
    public void tearDown() {
        houseHoldRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        ResultActions result = mockMvc.perform((get(MainController.HOME +
        DetailHouseHoldController.HOUSEHOLD_DETAILS + MainController.HOME + houseHold.getIdNumber())));
        assertAddFormDisplayed(result);
    }

    public void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(model().attribute(HouseHoldController.HOUSEHOLD, houseHold))
                .andExpect(model().attribute(DetailHouseHoldController.DEPENDANTS, dependants))
                .andExpect(view().name(DetailHouseHoldController.HOUSEHOLD_DETAILS))
                .andDo(print());
    }
}
