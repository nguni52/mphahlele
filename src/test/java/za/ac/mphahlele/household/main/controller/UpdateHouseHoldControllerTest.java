package za.ac.mphahlele.household.main.controller;


import org.apache.log4j.Logger;
import org.hamcrest.core.IsEqual;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import za.ac.mphahlele.builder.AddressBuilder;
import za.ac.mphahlele.builder.HouseHoldBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.HouseHoldRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Address;
import za.ac.mphahlele.model.HouseHold;
import za.ac.mphahlele.util.RequestUtils;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class UpdateHouseHoldControllerTest extends WebAppConfigurationAware {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    @Inject
    HouseHoldRepository houseHoldRepository;
    private HouseHold houseHold;
    private MockHttpServletRequestBuilder requestBuilder;
    private String houseHoldRequestString;
    @Before
    public void setUp() throws Exception {
        houseHold = HouseHoldBuilder.buildAHouseHold(1);
        houseHoldRepository.save(houseHold);
        houseHoldRequestString = MainController.HOME + UpdateHouseHoldController.HOUSEHOLD_UPDATE +
                MainController.HOME + houseHold.getIdNumber();
        requestBuilder = post(houseHoldRequestString);
    }

    @After
    public void tearDown() {
        houseHoldRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        ResultActions result = mockMvc.perform(get(houseHoldRequestString));
        assertAddFormDisplayed(result);
    }

    private void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(model().attribute(HouseHoldController.HOUSEHOLD,
                hasProperty("idNumber", IsEqual.equalTo(houseHold.getIdNumber()))))
                .andExpect(view().name(UpdateHouseHoldController.HOUSEHOLD_UPDATE));
    }

    @Test
    public void savesHouseHold() throws Exception {
        String expectedSurname = "Motho";
        String expectedFullName = "Nguni Oe";
        HouseHold expectedHouseHold = houseHold;
        expectedHouseHold.setSurname(expectedSurname);
        expectedHouseHold.setFullName(expectedFullName);
        addHouseHold(requestBuilder, expectedHouseHold);
        mockMvc.perform(requestBuilder);

        List<HouseHold> houseHolds = houseHoldRepository.findAll();
        assertEquals(1, houseHolds.size());
        assertEquals(expectedSurname, houseHolds.get(0).getSurname());
        assertEquals(expectedFullName, houseHolds.get(0).getFullName());
        logger.info(houseHolds.get(0).toString());
    }

    @Test
    public void catchException() throws Exception {
        houseHold.setIdNumber("");
        houseHoldRepository.save(houseHold);
        addHouseHold(requestBuilder, houseHold);
        ResultActions result = mockMvc.perform(requestBuilder);
        result.andExpect(model().hasErrors());
        assertAddFormDisplayed(result);
    }

    private void addHouseHold(MockHttpServletRequestBuilder requestBuilder, HouseHold houseHold) {
        requestBuilder.param("idNumber", houseHold.getIdNumber())
                .param("title", houseHold.getTitle())
                .param("surname", houseHold.getSurname())
                .param("fullName", houseHold.getFullName())
                .param("standNo", houseHold.getStandNo())
                .param("wardNumber", houseHold.getWardNumber())
                .param("levyAmount", Double.toString(houseHold.getLevyAmount()));
        Address postalAddress = AddressBuilder.buildAnAddress();
        RequestUtils.addPostalAddress(requestBuilder, postalAddress);
    }
}
