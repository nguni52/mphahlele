package za.ac.mphahlele.household.main.controller;

import org.junit.After;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import za.ac.mphahlele.builder.HouseHoldBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.HouseHoldRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.HouseHold;

import javax.inject.Inject;
import java.util.Collection;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class ListHouseHoldControllerTest extends WebAppConfigurationAware {

    @Inject
    private HouseHoldRepository houseHoldRepository;

    @After
    public void tearDown() throws Exception {
        houseHoldRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        int numberOfHouseHolds = 10;
        Collection<HouseHold> houseHolds = HouseHoldBuilder.buildHouseHolds(numberOfHouseHolds);
        houseHoldRepository.save(houseHolds);
        ResultActions result = mockMvc.perform(get(MainController.HOME +
                ListHouseHoldController.HOUSEHOLD_LIST));
        result.andExpect(model().attribute(ListHouseHoldController.HOUSEHOLDS, hasSize(numberOfHouseHolds)))
        .andExpect(view().name(ListHouseHoldController.HOUSEHOLD_LIST))
        ;
     }
}
