package za.ac.mphahlele.household.main.service;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.mockito.Mock;
import za.ac.mphahlele.builder.HouseHoldBuilder;
import za.ac.mphahlele.data.MockitoTest;
import za.ac.mphahlele.data.repository.HouseHoldRepository;
import za.ac.mphahlele.exception.EntityExistsException;
import za.ac.mphahlele.model.HouseHold;

import static org.mockito.Mockito.*;

public class HouseHoldTest extends MockitoTest {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    @Mock
    private HouseHoldRepository houseHoldRepository;

    @Test(expected = EntityExistsException.class)
    public void throwExceptionIfHouseHoldWithSameNameExists() {
        HouseHold houseHold = HouseHoldBuilder.buildAHouseHold(1);
        when(houseHoldRepository.findByIdNumber(houseHold.getIdNumber())).thenReturn(houseHold);
        HouseHoldServer houseHoldServer = new HouseHoldServer(houseHoldRepository);
        houseHoldServer.create(houseHold);
        verify(houseHoldRepository, never()).save(houseHold);
    }
}
