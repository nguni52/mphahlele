package za.ac.mphahlele.home;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import za.ac.mphahlele.config.LoggedonSecurityAware;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class MainControllerTest  extends LoggedonSecurityAware {
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Test
    public void displayVillageListIfAdministrator() throws Exception {
        ResultActions result = mockMvc.perform(get(MainController.HOME));
    }

    @Test
    public void displaySignIn() throws Exception {
        ResultActions result = mockMvc.perform(get(MainController.LOGIN));
    }

    @Test
    public void displayUserAuthenticatedButNoAuthority() throws Exception {

        //ResultActions result = mockMvc.perform(get(MainController.HOME));
    }
}

