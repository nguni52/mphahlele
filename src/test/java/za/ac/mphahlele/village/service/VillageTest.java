package za.ac.mphahlele.village.service;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.mockito.Mock;
import za.ac.mphahlele.builder.VillageBuilder;
import za.ac.mphahlele.data.MockitoTest;
import za.ac.mphahlele.data.repository.VillageRepository;
import za.ac.mphahlele.exception.EntityExistsException;
import za.ac.mphahlele.model.Village;

import static org.mockito.Mockito.*;

public class VillageTest extends MockitoTest {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    @Mock
    private VillageRepository villageRepository;

    @Test(expected = EntityExistsException.class)
    public void throwExceptionIfHeadmanWithSameIdNumberExists() {
        Village village = VillageBuilder.buildAVillage(1);
        when(villageRepository.findById(village.getId())).thenReturn(village);
        VillageServer villageServer = new VillageServer(villageRepository);
        villageServer.create(village);
        verify(villageRepository, never()).save(village);
    }
}
