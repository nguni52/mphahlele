package za.ac.mphahlele.village.controller;

import org.apache.log4j.Logger;
import org.hamcrest.core.IsEqual;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import za.ac.mphahlele.builder.VillageBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.VillageRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Village;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class UpdateVillageControllerTest extends WebAppConfigurationAware {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    @Inject
    VillageRepository villageRepository;
    private MockHttpServletRequestBuilder requestBuilder;
    private Village village;
    private String updateVillageRequest;

    @Before
    public void setUp() throws Exception {
        village = VillageBuilder.buildAVillage(1);
        villageRepository.save(village);
        updateVillageRequest = MainController.HOME + UpdateVillageController.VILLAGE_UPDATE +
                MainController.HOME + village.getId();
        requestBuilder = post(updateVillageRequest);
    }

    @After
    public void tearDown() {
        villageRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        ResultActions result = mockMvc.perform(get(updateVillageRequest));
        assertAddFormDisplayed(result);
    }

    private void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(model().attribute(VillageController.VILLAGE,
                hasProperty("name", IsEqual.equalTo((village.getName())))))
                .andExpect(view().name(UpdateVillageController.VILLAGE_UPDATE));
    }

    @Test
    public void savesChurch() throws Exception {
        String expectedHeadman = "Headman";
        String expectedPositionType = "Chief";
        Village expectedVillage = villageRepository.findByName("village1");
        logger.info(expectedVillage);
        expectedVillage.setHeadman(expectedHeadman);
        expectedVillage.setPositionType(expectedPositionType);
        addVillage(requestBuilder, expectedVillage);
        mockMvc.perform(requestBuilder).andDo(print());

        List<Village> villages = villageRepository.findAll();
        assertEquals(1, villages.size());
        assertEquals(expectedHeadman, villages.get(0).getHeadman());
        assertEquals(expectedPositionType, villages.get(0).getPositionType());
    }

    @Test
    public void validateName() throws Exception {
        village.setName("");
        villageRepository.save(village);
        addVillage(requestBuilder, village);
        ResultActions result = mockMvc.perform(requestBuilder);
        result.andExpect(model().hasErrors());
        assertAddFormDisplayed(result);
    }

    @Test
    public void catchException() throws Exception {
        assertNotNull(villageRepository);
    }

    private void addVillage(MockHttpServletRequestBuilder requestBuilder, Village village) {
        requestBuilder.param("id", village.getId())
                .param("name", village.getName())
                .param("headman", village.getHeadman())
                .param("positionType", village.getPositionType())
                .param("contactDetails", village.getContactDetails());
    }
}
