package za.ac.mphahlele.village.controller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import za.ac.mphahlele.builder.VillageBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.VillageRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Village;
import za.ac.mphahlele.support.Message;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CreateVillageControllerTest extends WebAppConfigurationAware {
    @Inject
    private VillageRepository villageRepository;
    private MockHttpServletRequestBuilder requestBuilder;
    private Village village;

    @Before
    public void setUp() throws Exception {
        village = VillageBuilder.buildAVillage(1);
        requestBuilder = post(MainController.HOME + CreateVillageController.VILLAGE_CREATE);
    }

    @After
    public void tearDown() throws Exception {
        villageRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        ResultActions result = mockMvc.perform(get(MainController.HOME +
                CreateVillageController.VILLAGE_CREATE));
        assertAddFormDisplayed(result);
    }

    private void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(view().name(CreateVillageController.VILLAGE_CREATE));
    }

    @Test
    public void savesVillage() throws Exception {
        addVillage(requestBuilder, village);
        mockMvc.perform(requestBuilder)
                .andExpect(redirectedUrl(MainController.HOME +  ListVillageController.VILLAGE_LIST))
                .andExpect(flash().attribute(Message.MESSAGE_ATTRIBUTE,
                        hasProperty("type", equalTo(Message.Type.SUCCESS))));

        List<Village> villageList = villageRepository.findAll();
        assertEquals(1, villageList.size());
        assertEquals(village.getName(), villageList.get(0).getName());
        assertEquals(village.getHeadman(), villageList.get(0).getHeadman());
    }

    private void addVillage(MockHttpServletRequestBuilder requestBuilder, Village village) {
        requestBuilder.param("name", village.getName())
                .param("headman", village.getHeadman())
                .param("positionType", village.getPositionType())
                .param("contactDetails", village.getContactDetails());
    }
}
