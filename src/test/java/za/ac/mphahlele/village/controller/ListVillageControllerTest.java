package za.ac.mphahlele.village.controller;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import za.ac.mphahlele.builder.VillageBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.VillageRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Village;

import javax.inject.Inject;
import java.util.Collection;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class ListVillageControllerTest extends WebAppConfigurationAware {
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Inject
    private VillageRepository villageRepository;

    @After
    public void tearDown() throws Exception {
        villageRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        int numberOfVillages = 10;
        Collection<Village> villages = VillageBuilder.buildVillages(numberOfVillages);
        for(Village village: villages) {
            logger.info(village.toString());
        }
        villageRepository.save(villages);
        ResultActions result = mockMvc.perform(get(MainController.HOME +
                ListVillageController.VILLAGE_LIST));
        //result.andExpect(model().attribute(ListVillageController.VILLAGE_LIST, hasSize(numberOfVillages)));
        result.andExpect(view().name(ListVillageController.VILLAGE_LIST));
    }
}