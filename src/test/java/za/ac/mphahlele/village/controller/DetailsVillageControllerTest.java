package za.ac.mphahlele.village.controller;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import za.ac.mphahlele.builder.VillageBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.VillageRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Village;

import javax.inject.Inject;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class DetailsVillageControllerTest extends WebAppConfigurationAware {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private Village village;

    @Inject
    private VillageRepository villageRepository;

    @Before
    public void setUp() throws Exception {
        village = VillageBuilder.buildAVillage(1);
        villageRepository.save(village);
    }

    @After
    public void tearDown() {
        villageRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        logger.info(village.toString());
        ResultActions result = mockMvc.perform((get(MainController.HOME +
                DetailVillageController.VILLAGE_DETAILS + MainController.HOME + village.getId())));
        assertAddFormDisplayed(result);
    }

    public void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(model().attribute(DetailVillageController.VILLAGE, village))
                .andExpect(view().name(DetailVillageController.VILLAGE_DETAILS));
    }
}