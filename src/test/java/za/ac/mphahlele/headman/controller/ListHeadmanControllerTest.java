package za.ac.mphahlele.headman.controller;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import za.ac.mphahlele.builder.HeadmanBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.HeadmanRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Headman;

import javax.inject.Inject;
import java.util.Collection;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class ListHeadmanControllerTest extends WebAppConfigurationAware {
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Inject
    private HeadmanRepository headmanRepository;


    @After
    public void tearDown() throws Exception {
        headmanRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        int numberOfHeadmen = 10;
        Collection<Headman> headmen = HeadmanBuilder.buildHeadmen(numberOfHeadmen);
        for(Headman headman: headmen) {
            logger.info("HEADMAN: " + headman.toString());
        }
        headmanRepository.save(headmen);
        ResultActions result = mockMvc.perform(get(MainController.HOME +
                ListHeadmanController.HEADMAN_LIST));
        result.andExpect(model().attribute(ListHeadmanController.HEADMEN, hasSize(numberOfHeadmen)));
        result.andExpect(view().name(ListHeadmanController.HEADMAN_LIST));
    }
}
