package za.ac.mphahlele.headman.controller;

import org.apache.log4j.Logger;
import org.hamcrest.core.IsEqual;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import za.ac.mphahlele.builder.AddressBuilder;
import za.ac.mphahlele.builder.HeadmanBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.HeadmanRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Address;
import za.ac.mphahlele.model.Headman;
import za.ac.mphahlele.util.RequestUtils;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 * Created by nguni52 on 14/12/26.
 */
public class UpdateHeadmanControllerTest extends WebAppConfigurationAware {
    private Logger logger = Logger.getLogger(this.getClass().getName());

    @Inject
    HeadmanRepository headmanRepository;
    private MockHttpServletRequestBuilder requestBuilder;
    private Headman headman;
    private String updateHeadmanRequest;

    @Before
    public void setUp() throws Exception {
        headman = HeadmanBuilder.buildAHeadman(1);
        headmanRepository.save(headman);
        updateHeadmanRequest = MainController.HOME + UpdateHeadmanController.HEADMAN_UPDATE +
                MainController.HOME + headman.getIdNumber();
        requestBuilder = post(updateHeadmanRequest);
    }

    @After
    public void tearDown() {
        headmanRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        ResultActions result = mockMvc.perform(get(updateHeadmanRequest));
        assertAddFormDisplayed(result);
    }

    private void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(model().attribute(HeadmanController.HEADMAN,
                hasProperty("name", IsEqual.equalTo((headman.getName())))))
                .andExpect(view().name(UpdateHeadmanController.HEADMAN_UPDATE));
    }

    @Test
    public void savesHeadman() throws Exception {
        String expectedName = "headman1";
        String expectedUnitNumber = "5";
        Headman expectedHeadman = headmanRepository.findByName("headman1");
        logger.info(expectedHeadman);
        expectedHeadman.setName(expectedName);
        expectedHeadman.setUnitNumber(expectedUnitNumber);
        addHeadman(requestBuilder, expectedHeadman);
        mockMvc.perform(requestBuilder).andDo(print());

        List<Headman> headmanList = headmanRepository.findAll();
        assertEquals(1, headmanList.size());
        assertEquals(expectedName, headmanList.get(0).getName());
        assertEquals(expectedUnitNumber, headmanList.get(0).getUnitNumber());
    }

    @Test
    public void validateName() throws Exception {
        headman.setName("");
        headmanRepository.save(headman);
        addHeadman(requestBuilder, headman);
        ResultActions result = mockMvc.perform(requestBuilder);
        result.andExpect(model().hasErrors());
        assertAddFormDisplayed(result);
    }

    @Test
    public void catchException() throws Exception {
        assertNotNull(headmanRepository);
    }

    private void addHeadman(MockHttpServletRequestBuilder requestBuilder, Headman headman) {
        requestBuilder.param("idNumber", headman.getIdNumber())
                .param("name", headman.getName())
                .param("unitNumber", headman.getUnitNumber())
                .param("wardNumber", headman.getWardNumber());
        Address postalAddress = AddressBuilder.buildAnAddress();
        RequestUtils.addPostalAddress(requestBuilder, postalAddress);
    }
}
