package za.ac.mphahlele.headman.controller;

import org.apache.log4j.Logger;
import org.hamcrest.beans.HasPropertyWithValue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import za.ac.mphahlele.builder.HeadmanBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.HeadmanRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Address;
import za.ac.mphahlele.model.Headman;
import za.ac.mphahlele.support.Message;
import za.ac.mphahlele.util.RequestUtils;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CreateHeadmanControllerTest extends WebAppConfigurationAware {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    @Inject
    private HeadmanRepository headmanRepository;
    private MockHttpServletRequestBuilder requestBuilder;
    private Headman headman;

    @Before
    public void setUp() throws Exception {
        headman = HeadmanBuilder.buildAHeadman(1);
        requestBuilder = post(MainController.HOME + CreateHeadmanController.HEADMAN_CREATE);
    }

    @After
    public void tearDown() {
        headmanRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {
        ResultActions result = mockMvc.perform(get(MainController.HOME +
        CreateHeadmanController.HEADMAN_CREATE));
        assertAddFormDisplayed(result);
    }

    private void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(view().name(CreateHeadmanController.HEADMAN_CREATE));
    }

    @Test
    public void savesHeadmen() throws Exception {
        addHeadmen(requestBuilder, headman);
        mockMvc.perform(requestBuilder)
                .andExpect(redirectedUrl(MainController.HOME
                + ListHeadmanController.HEADMAN_LIST))
                .andExpect(flash().attribute(Message.MESSAGE_ATTRIBUTE,
                        HasPropertyWithValue.hasProperty("type", equalTo(Message.Type.SUCCESS))));

        List<Headman> headmanList = headmanRepository.findAll();
        assertEquals(1, headmanList.size());
        assertEquals(headman, headmanList.get(0));
    }

    private void addHeadmen(MockHttpServletRequestBuilder requestBuilder, Headman headman) {
        Address postalAddress = headman.getPostalAddress();
        requestBuilder.param("idNumber", headman.getIdNumber())
                .param("name", headman.getName())
                .param("gender", headman.getGender())
                .param("unitNumber", headman.getUnitNumber())
                .param("wardNumber", headman.getWardNumber());
        RequestUtils.addPostalAddress(requestBuilder, postalAddress);
    }
}
