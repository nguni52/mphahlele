package za.ac.mphahlele.headman.controller;


import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.ResultActions;
import za.ac.mphahlele.builder.HeadmanBuilder;
import za.ac.mphahlele.config.WebAppConfigurationAware;
import za.ac.mphahlele.data.repository.HeadmanRepository;
import za.ac.mphahlele.home.MainController;
import za.ac.mphahlele.model.Headman;

import javax.inject.Inject;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class DetailHeadmanControllerTest extends WebAppConfigurationAware {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private Headman headman;

    @Inject
    private HeadmanRepository headmanRepository;

    @Before
    public void setUp() throws Exception {
        headman = HeadmanBuilder.buildAHeadman(1);logger.info(headman.toString());
        headmanRepository.save(headman);
    }

    @After
    public void tearDown() throws Exception {
        headmanRepository.deleteAll();
    }

    @Test
    public void getCorrectViewAndModelAttribute() throws Exception {logger.info(headman.toString());
        logger.info("\n\n\n\n******:::" + headman.getIdNumber() + "\n\n\n\n\n");
        ResultActions result = mockMvc.perform(get(MainController.HOME +
                DetailHeadmanController.HEADMAN_DETAILS + MainController.HOME + headman.getIdNumber()));
        assertAddFormDisplayed(result);
    }

    public void assertAddFormDisplayed(ResultActions result) throws Exception {
        result.andExpect(model().attribute(HeadmanController.HEADMAN, headman))
                .andExpect(view().name(DetailHeadmanController.HEADMAN_DETAILS));
    }
}
