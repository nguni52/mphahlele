package za.ac.mphahlele.headman.service;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.mockito.Mock;
import za.ac.mphahlele.builder.HeadmanBuilder;
import za.ac.mphahlele.data.MockitoTest;
import za.ac.mphahlele.data.repository.HeadmanRepository;
import za.ac.mphahlele.exception.EntityExistsException;
import za.ac.mphahlele.model.Headman;

import static org.mockito.Mockito.*;

public class HeadmanTest extends MockitoTest {
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    @Mock
    private HeadmanRepository headmanRepository;

    @Test(expected = EntityExistsException.class)
    public void throwExceptionIfHeadmanWithSameIdNumberExists() {
        Headman headman = HeadmanBuilder.buildAHeadman(1);
        when(headmanRepository.findByIdNumber(headman.getIdNumber())).thenReturn(headman);
        HeadmanServer headmanServer = new HeadmanServer(headmanRepository);
        headmanServer.create(headman);
        verify(headmanRepository, never()).save(headman);
    }
}
