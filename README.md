# README #

In order to get this up and running, you will obviously need git, tomcat, maven and mongodb

### What is this repository for? ###

* Quick summary: This system will manage people in a village and the levies they are supposed to pay
* Version: Alpha 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up - Make sure that git, tomcat, maven and mongodb and java are installed
* Configuration - Change the path to ${tomcat.home} in pom.xml to your local tomcat home
* Dependencies
* Database configuration - Insert users from Accounts file into the mongo database to be able to login 
* How to run tests - mvn clean test will run the tests and generate cobertura reports for code coverage
* Deployment instructions - to deploy, use mvn clean package -DskipTests -Pdev if profile id dev, and local if profile if local and prod if profile is production

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* nguni52/lineopitso
* Other contacts: Anna